//Java program to illustrate 
//method overloading in varargs 
public class varargsDemo 
{ 
    public static void main(String[] args) 
    { 
         fun(); 
    }
    //varargs method with float datatype 
    static void var(float  x) 
    { 
        System.out.println("float varargs"); 
    } 
    
    //varargs method with int datatype 
    static void var(int x) 
    { 
        System.out.println("int varargs"); 
    } 
    
    //varargs method with double datatype 
    static void var(double x) 
    { 
        System.out.println("double varargs"); 
    } 
}
