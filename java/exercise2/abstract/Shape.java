+ Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively



import java.io.*;
import java.util.*;
abstract class Shape {
    abstract public double printArea();
    abstract public double printPeriMeter();
}
class Square extends Shape {
    private double side;
    public Square(double side) {
        this.side= side;
    }
    public double printArea() {
        return side*side;
    }
    public double printPeriMeter() {
        return 4*side;
    }
}

class Circle extends Shape {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double printArea() {
        return (3.14f*(radius*radius));
    }
    public double printPeriMeter() {
        return 2*3.14f*radius;
    }
}

public class Jmain
{
    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);   
        System.out.println("Choose the Shape");
        System.out.println("1. Square 2.Circle");
        int cho = scan.nextInt();
        switch(cho)
        {
            case 1:
                System.out.println("Enter the length");
                int side = scan.nextInt();
                Square square =new Square(side);
                System.out.println("The Area of the Square is "+ square.printArea());
                System.out.println("The Perimeter of the Square is "+ square.printPeriMeter());
                break;
            case 2:
                System.out.println("Enter the radius");
                int radius = scan.nextInt(); 
                Circle circle =new Circle(radius);          
                System.out.println("The Area of the Circle is "+ circle.printArea());
                //System.out.println("ThePerimeter of the Circle is "+ circle.printPeriMeter());
                break;
        }    
    }
}