/*
Requirements:
    + What is wrong with the following interface? and fix it.
        public interface SomethingIsWrong {
            void aMethod(int aValue){
                System.out.println("Hi Mom");
            }
        }
	
Entities:
    - interface Parent
	- class Child
	
Function Declaration:
    - void display(int value)
	- public static void main(String[] args)
	
Job to be done:
    - Create a interface Parent
	- Declare the functions in Parent interface
	- Create a class Child that implements Parent interface
	- Define the functions of Parent interface in the Child class
	- Create class CorrectProgram
	- Create object obj which is instance of Child class
    - Declare a main method
	- Invoke the functions of Parent interface in init

Explanation:
    Function can only be declared in an interface. 
	Function cannot be defined in an interface
	Functions of interface can be defined in the class which implements it

*/

package com.kpriet.training.java.interfc;

interface Parent {
	
	void display(int value);
}

class Child implements Parent {
	
	public void display(int value) {
		System.out.println("Hello, The value is " + value);
	}
}

public class CorrectProgram {
	
	public static void main(String[] args) {
		Child obj = new Child();
		obj.display(5);
	}
}



















