3)What's wrong with the following program?

public class SomethingIsWrong {
    public static void main(String[] args) {
        Rectangle myRect;
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}

    
Word Breakdown Structure(WBS)
1.Requirments
    Fix the erorr in the code.

2.Entities
    class : Rectangle

3.Function Declaration
    public class Rectangle()
    public int area()

4.Jobs to be done
    (i).Create an class and Declare instance variable inside the class.
    (ii).Inside the class declare datatype method with two parameters for return the vaules and 
declaring main method.
    (iii).In the main method create an object and initializing values using two object with variables
    (iv).In the print statement invoking a function when passing two arguments.

CODE:

public class Rectangle {          //Class name changed as Rectangle
    public int width;                    //Initialized two instances variables width and height
    public int height;
    public int area(int width,int height) {   // Declared method in the given datatype with parameters
        return width*height;                           // returning rectangle calculated value 
    }
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle(); // creating an myRect object 
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area(myRect.width,myRect.height)); // calling method with two arguments
        }
    }
    