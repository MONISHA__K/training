2)What is the output from the following code:

IdentifyMyParts a = new IdentifyMyParts(); 
IdentifyMyParts b = new IdentifyMyParts(); 
a.y = 5; 
b.y = 6; 
a.x = 1; 
b.x = 2; 
System.out.println("a.y = " + a.y); 
System.out.println("b.y = " + b.y); 
System.out.println("a.x = " + a.x); 
System.out.println("b.x = " + b.x); 
System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);


Word Breakdown Structure(WBS)
1.Requirments
    Check an output of the code.
2.Entities
    IdentifyMyParts
3.Function Declaration
    public class IdentifyMyParts()
4.Jobs to be done
    (i).Create an class.
    (ii).Declare two variables inside the class as
            *class variable
            *instance variable
    (iii).Declare main method inside the class
    (iv).In the main method create two object and initializing values using two object with variables
    (v).Print the initialized values using object and one using class name

Since x is a static variable ,the value which is assigned last will has it effect on all the instances of the class

code:

public class IdentifyMyParts {
    public static int x = 7;
    public int y = 3;
    public static void main(String args[]){
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y);
        System.out.println("b.y = " + b.y);
        System.out.println("a.x = " + a.x);
        System.out.println("b.x = " + b.x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
        }
    }


Answer: Here is the output:

 a.y = 5 
 b.y = 6 
 a.x = 2 
 b.x = 2
 IdentifyMyParts.x = 2

