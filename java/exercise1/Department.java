package com.kpriet.training.interfaces;
abstract interface Department {
    public void detail(String deptName,String deptCode);
    public void year(String className);
}