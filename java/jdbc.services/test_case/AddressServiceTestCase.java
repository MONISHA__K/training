
/*
Requirement:
    To test the conditions in the AddressService.
    
Entity:
    1.AddressServiceTestCase
    2.AddressService
    3.AppException
    4.Address

Function declaration:
    public void setUp()
    public void addressCreationTest()
    public void addressCreationTest1()
    public void addressReadTest()
    public void addressReadTest1()
    public void addressReadAllTest()  
    public void addressUpdationTest()
    public void addressUpdationTest1()
    public void addressUpdateTest2()
    public void addressDeleteTest()
    public void addressDeleteTest1()
    void afterTestCases()
    
Jobs to be done :
    1.create an instance for ConnectionService and AddressService
    2.Declare address ,address1,updatedAddress,expectedAddress ,address2 as type Address in private.
    3.Declare id as type long in private.
    4.Inside a setUp() method 
       4.1) invoke the connectionService using init() method from ConnectionService class.
       4.2) Pass the new address which you want to create, as a parameter to Address class.
       4.3) Store the new address in  address ,address1,updatedAddress,expectedAddress ,address2.
    
    5.The first priority is to check "Address Creation with postal_code not as 0"
       5.1)using the addressCreationTest() method 
           5.1.1)Invoke addressService.create() method from AddressService ,pass the connection and address1 as  parameter to it.
                 and store it in id .
           5.1.2)Inside try block ,After storing in id commit the connection using commit()
           5.1.3)If any error caused catch block catch the error and print it.
           5.1.4)Check whether the id is greater than zero.using Assert 
    
    6.The second priority is to check "Address Creation with postal_code as 0"
        6.1) using the addressCreationTest1() method 
             6.1.1)Check if the postal_code is zero and it throws AppException.
             6.1.2)Invoke addressService.create() method from AddressService ,
                   pass the connection and address1 as  parameter to it.
    
    7.The third priority is to Read address with the given id
      using addressReadTest() method 
        7.1 check if the id is present 
            7.1.1)If present Invoke addressService.read()) method from AddressService ,pass the connection and id as  parameter to it.
                  and store it in expectedAddress .
            7.1.2)Get the street,city and postalCode from address and expectedAddress
            7.1.3)Check both are equal or not
        7.2  if id not present throw AppException
    
    8.The fourth priority is to check the id is not valid 
      using addressReadTest1() 
      8.1) Invoke addressService.read() method from AddressService ,pass the connection and invalid id as  parameter to it.
           check it is null or not using assertTrue
    
    9.The fifth is to check it reads all address 
      using addressReadAllTest() method
         9.1)Invoke addressService.read()) method from AddressService 
             ,pass the connection as  parameter to it. 
         9.2)And check the value not equals to null 
             9.2.1) if null throw AppException.
    
    10.The sixth priority is to Address Updation with postal_code not as 0 and valid id
       using addressUpdationTest() method 
       10.1)check the address is updated or not
            10.1.1)Invoke addressService.update()) method from AddressService 
                  ,pass the connection and id and address that you want to update as  parameter to it.
            10.1.2) Invoke addressService.read()) method from AddressService 
                  ,pass the connection and id and that you updated as  parameter to it.  
                   and store it in expectedAddress .
            10.1.3)Get the street,city ,id and postalCode from updateAddress and expectedAddress
            10.1.4)Check both are equal or not
            10.1.5) If  not updated throw AppException              
    
    11. The seventh priority is to Address Updation with postal_code as 0 .
        using addressUpdationTest1() method 
        11.1)Invoke addressService.update()) method from AddressService  
             ,pass the connection and id and address that you want to update, as  parameter to it.
        11.2) if postalCode is zero in address that you want to update it throw AppException
    
    12. The nineth priority is to Address Updation with invalid id.
        using addressUpdateTest2()
        12.1)Invoke addressService.update()) method from AddressService  
            ,pass the connection and id and address that you want to update, as  parameter to it.
        12.2) if id is not in address , it throw AppException.
    
    13. The tenth priority is to Delete address with valid id
        using addressDeleteTest() method
        13.1) Invoke addressService.delete()) method from AddressService  
            ,pass the connection and id and address which you want to delete, as  parameter to it.
        13.2) if id is not in address , it throw AppException.
    
    14. The eleventh priority is to Delete address with invalid id
        using addressDeleteTest() method
        14.1) Invoke addressService.delete()) method from AddressService  
              ,pass the connection and id and address which you want to delete, as  parameter to it.
        14.2) if id is not in address , it throw AppException.
        
    15. using afterTestCases() method close the connectionService by invoking release().

pseudo code :

    class AddressServiceTestCase {
        
        private ConnectionService connectionService = new ConnectionService();
        private AddressService addressService = new AddressService();
        private Address address;
        private Address address1;
        private Address updatedAddress;
        private Address expectedAddress;
        private Address address2;
        private long id;
        
        @BeforeClass
        public void setUp() {
            connectionService.init();
            address = new Address("MG Road", "Bangalore", 628402);
            address1 = new Address("Bebngal street", "WestBengal", 0);
            updatedAddress = new Address("MGR Road", "Chennai", 628400);
            address2 = new Address("942,f/6, SVL Nagar, Manthithoppu", "Kovilpatti", 628502);
            
        }
    
        @Test(priority = 1, description = "Address Creation with postal_code not as 0")
        public void addressCreationTest() {
            this.id = addressService.create(connectionService.con, address);
            connectionService.commit();
            Assert.assertTrue(this.id > 0);
        }
        
        @Test(priority = 2, description = "Address Creation with postal_code as 0", expectedExceptions = AppException.class, expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
        public void addressCreationTest1() throws AppException {
            addressService.create(connectionService.con, address1);
        }
        
        @Test(priority = 3, description = "Reading address with the given id")
        public void addressReadTest() throws AppException {
            expectedAddress = addressService.read(connectionService.con, this.id);
            Assert.assertEquals(expectedAddress.getStreet(), address.getStreet());
            Assert.assertEquals(expectedAddress.getCity(), address.getCity());
            Assert.assertEquals(expectedAddress.getPostalCode(), address.getPostalCode());
        }
        
        @Test(priority = 4, description = "invalid id")
        public void addressReadTest1() throws AppException {
            Assert.assertEquals(addressService.read(connectionService.con, 0), null);
        }
        
        @Test(priority = 5, description = "Reading all address")
        public void addressReadAllTest() throws AppException {
            Assert.assertTrue(addressService.readAll(connectionService.con) != null);
        }
        
        @Test(priority = 5, description = "Address Updation with postal_code not as 0 and valid id")
        public void addressUpdationTest() throws AppException {
            addressService.update(connectionService.con, this.id, updatedAddress);
            connectionService.commit();
            expectedAddress = addressService.read(connectionService.con, this.id);
            Assert.assertEquals(updatedAddress.getStreet(), expectedAddress.getStreet());
            Assert.assertEquals(updatedAddress.getCity(), expectedAddress.getCity());
            Assert.assertEquals(updatedAddress.getPostalCode(), expectedAddress.getPostalCode());
        }
        
        @Test(priority = 7, description = "Address Updation with postal_code as 0", expectedExceptions = AppException.class, expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
        public void addressUpdationTest1() throws AppException {
            addressService.update(connectionService.con, 13, address1);
        }
        
        @Test(priority = 9, description = "Updating address with invalid id", expectedExceptions = AppException.class, expectedExceptionsMessageRegExp = "ERR404 : Failed to update Address")
        public void addressUpdateTest2() throws AppException {
            addressService.update(connectionService.con, 100, address2);
        }
        
        @Test(priority = 10, description = "Deleting address with valid id")
        public void addressDeleteTest() throws AppException {
            addressService.delete(connectionService.con, this.id);
            Assert.assertEquals(addressService.read(connectionService.con, this.id), null);
        }
        
        @Test(priority = 11, description = "Deleting address with invalid id", expectedExceptions = AppException.class, expectedExceptionsMessageRegExp = "ERR405 : Failed to delete Address")
        public void addressDeleteTest1() throws AppException {
            addressService.delete(connectionService.con, 100);
        }
        
        @AfterClass
        void afterTestCases() {
            connectionService.release();
        }
    }

*/
package com.kpr.training.jdbc.test_case;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jdbc.service.ConnectionService;

public class AddressServiceTestCase {
    
    private AddressService addressService = new AddressService();
    private Address address;
    private Address address1;
    private Address updatedAddress;
    private Address expectedAddress;
    private Address address2;
    private long id;

    @BeforeClass(groups = {"createTest", "readTest", "readAllTest", "updateTest", "deleteTest", "searchTest"})
    public void setUp() {

        new ConnectionService().init();
        address = new Address("MG Road", "Bangalore", 628402);
        address1 = new Address("Bebngal street", "WestBengal", 0);
        updatedAddress = new Address("MGR Road", "Chennai", 628400);
        address2 = new Address("942,f/6, SVL Nagar, Manthithoppu", "Kovilpatti", 628502);
    }

    @Test(groups = {"createTest"}, priority = 1, description = "Address Creation with postal_code not as 0")
    public void addressCreationTest() {
        this.id = addressService.create(address);
        ConnectionService.commit();
        address.setId(this.id);
        Assert.assertEquals(address.toString(), addressService.read(this.id).toString());
    
    }

    @Test(groups = {"createTest"}, priority = 2, description = "Address Creation with postal_code as 0",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
    public void addressCreationTest1() throws AppException {
        addressService.create(address1);
    }

    @Test(groups = {"readTest"}, priority = 3, description = "Reading address with the given id")
    public void addressReadTest() throws AppException {
        expectedAddress = addressService.read(this.id);
        Assert.assertEquals(expectedAddress.toString(), address.toString());
    }

    @Test(groups = {"readTest"}, priority = 4, description = "invalid id")
    public void addressReadTest1() throws AppException {
        Assert.assertEquals(addressService.read(1000), null);
    }

    @Test(groups = {"readAllTest"}, priority = 5, description = "Reading all address")
    public void addressReadAllTest() throws AppException {
        int size = 0;
        try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_TABLE_SIZE)) {
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                size = result.getInt("COUNT(*)");
            }
        }catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_GET_ADDRESS_SIZE, e);
        }
        Assert.assertEquals(addressService.readAll().size(), size);
    }

    @Test(groups = {"updateTest"}, priority = 6, description = "Address Updation with postal_code not as 0 and valid id")
    public void addressUpdationTest() throws AppException {
        updatedAddress.setId(this.id);
        
        addressService.update(updatedAddress);
        ConnectionService.commit();
        expectedAddress = addressService.read(this.id);
        Assert.assertEquals(updatedAddress.toString(), expectedAddress.toString());
    }

    @Test(groups = {"updateTest"}, priority = 7, description = "Address Updation with postal_code as 0",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
    public void addressUpdationTest1() throws AppException {
        address1.setId(13);
        addressService.update(address1);
    }

    @Test(groups = {"updateTest"}, priority = 9, description = "Updating address with invalid id",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR404 : Failed to update Address")
    public void addressUpdateTest2() throws AppException {
        address2.setId(100);
        addressService.update(address2);
    }

    @Test(groups = {"deleteTest"}, priority = 10, description = "Deleting address with valid id")
    public void addressDeleteTest() throws AppException {
        addressService.delete(this.id);
        ConnectionService.commit();
        Assert.assertEquals(addressService.read(this.id), null);
    }

    @Test(groups = {"deleteTest"}, priority = 11, description = "Deleting address with invalid id",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR405 : Failed to delete Address")
    public void addressDeleteTest1() throws AppException {
        addressService.delete(100);
    }
    
    @Test(groups = {"searchTest"}, priority = 12, description = "Searching address with one property")
    public void addressSearch() throws AppException {
        addressService.search("", "", "");
    }
    
    @Test(groups = {"searchTest"}, priority = 13, description = "Searching address with combination of properties")
    public void addressSearch1() throws AppException {
        addressService.search("", "", "");
    }

    @AfterClass(groups = {"createTest", "readTest", "readAllTest", "updateTest", "deleteTest", "searchTest"})
    void afterTestCases() {
        ConnectionService.release();
    }
}

