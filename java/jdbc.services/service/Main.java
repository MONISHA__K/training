package com.kpr.training.jdbc.service;


import java.sql.Date;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class Main {
    //AddressService addressService = new AddressService();
    static PersonService personService = new PersonService();
    
    public static void main(String[] args) {
        Person person = new Person("Monisha", "18ec077@kpriet.ac.in", Date.valueOf("2019-02-25"));
        Address address = new Address("Seranmadevi", "Tirunelveli", 641652);
        personService.create(person, address);
        //AddressService.read(5);
    }
}