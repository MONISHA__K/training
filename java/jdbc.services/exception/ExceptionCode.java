package com.kpr.training.jdbc.exception;

public enum ExceptionCode implements ErrorCode {

	POSTAL_CODE_ZERO(CODE_001, "postal code should not be zero"), 
	SQLException(CODE_002,"SQL exception occurs"), 
	ADDRESS_CREATION_FAILS(CODE_003, "Address was not created");

	private final int id;
	private final String msg;

	ExceptionCode(int id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	public int getId() {
		return this.id;
	}

	public String getMsg() {
		return this.msg;
	}
}
