/*
 * Requirement:
 * 		To write the AppException for the person and address service
 * 
 * Entity:
 * 		AppException
 * 
 * Method Signature:
 * 		public AppException(ExceptionCode code)
 * 
 * Jobs To Be Done:
 * 		1) The respective errors using its code number are declared in enum.
 * 		2) The constructor is created with reference to error thrown from address and person service.
 * 		3) The respective error message is printed.
 * 
 * Pseudo code:
 * 
 *  enum ErrorCode {
 *  	//create the respective code and its message.
 *  	TYPE_OF_ERRORCODE("ERR401", "Error Message");
 *  }
 *  
 *  class AppException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    public AppException(ErrorCode code, Exception e) {
        super(code.getCode() + " : " + code.getMessage());
        e.printStackTrace();
    }
    
    public AppException(ErrorCode code) {
        super(code.getCode() + " : " + code.getMessage());
        System.out.println(
                code.getCode() + " : " + code.getMessage());
    }

}
 *  
 */
package com.kpr.training.jdbc.exception;

public class AppException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AppException(ErrorCode code, Exception e) {
        super(code.getCode() + " : " + code.getMessage());
        System.out.println(code.getCode() + " : " + code.getMessage());
        e.printStackTrace();
    }

    public AppException(ErrorCode code) {
        super(code.getCode() + " : " + code.getMessage());
        System.out.println(code.getCode() + " : " + code.getMessage());
    }

}
