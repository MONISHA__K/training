+ The following code creates one array and one string object. How many references to those objects
exist after the code executes?
  Is either object eligible for garbage collection?
    
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;

Requirement:
   The following code creates one array and one string object. How many references to those objects 
exist after the code executes? Is either object eligible for garbage collection?
    
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;

Entities:
   No class is used in this program

Function Declaration:
   No function declaration is declared in this program

Job to be done:
   1. Find the references to those objects that exist after the code executes
   2. Write the solution

Solution:
 There is one refernce  for the object after the code gets executed.The object is not eligible for 
garbage collection because it has already refernce to another object .