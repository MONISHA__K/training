package com.java.training.core.string;
/* + In the following program, called ComputeResult, what is the value of result after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /*1*/   result.setCharAt(0, original.charAt(0));
    /*2*/   result.setCharAt(1, original.charAt(original.length()-1));
    /*3*/   result.insert(1, original.charAt(4));
    /*4*/   result.append(original.substring(1,4));
    /*5*/   result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }
 Problem Statement
    1.what is the value of result after each numbered line executes?
	
Entity
    1.ComputeResult 
	
Jobs to be  Done
    1.Used StringBuilder class to build a mutable String.
    2.Getting the index of the character at a.
    3.Using the setCharAt Function of StringBuilder to replace the character of the String
      with the given Character at the given index position.
    4.Using the insert Function of the StringBuilder to insert a character or a given String
      at the index position specified in the String..
    5.Using Append Function of StringBuilder to append the given character or String at the 
      end of the existing String..
      
*/

public class ComputeResult {
    public void main(String[] args) {
        String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');
        
        /*1*/ result.setCharAt(0, original.charAt(0));
        //System.out.println(result); //OUTPUT : si//
        
        /*2*/ result.setCharAt(1, original.charAt(original.length()-1));
        //System.out.println(result); //OUTPUT : se//
        
        /*3*/ result.insert(1, original.charAt(4));
        //System.out.println(result); //OUTPUT : swe//
        
        /*4*/ result.append(original.substring(1,4));
        //System.out.println(result); //OUTPUT : sweoft//
        
        /*5*/ result.insert(3, (original.substring(index, index+2) + " "));
        //System.out.println(result); //OUTPUT : swear oft//
        
        System.out.println(result); //OUTPUT : swear oft//
    }
 }
 