package com.java.training.core.string;

/*+ Write a program that computes your initials from your full name and displays them.

    Problem Statement
    1.To write a program that computes your initials 
      from your full name and displays them.
    
    Entity
    1.NameBuilder Class
    
    Jobs to be  Done
    1.Create a class called NameReverse
    2.Get the name from the User.
    3.Using substring function of java.util slicing the 
      given input from 0th index to the 3rd index to get the Initial 
      of the Name Given..
*/

import java.util.Scanner;
public class NameReverse {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your Full Name");
        String name = scan.nextLine();
        //INTIAL SEPERTOR//
        System.out.println("Initial :");
        System.out.println(name.substring(0,2));
        //NAME SEPERATOR//
        System.out.println("Name :");
        System.out.println(name.substring(2,name.length()));
        
    }
}