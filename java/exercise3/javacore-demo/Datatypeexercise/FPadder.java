package com.java.training.core.Datatypeexercise;
/*Create a program that is similar to the previous one but has the following differences:

Instead of reading integer arguments, it reads floating-point arguments.
It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
For example, suppose that you enter the following:

java FPAdder 1 1e2 3.0 4.754
The program would display 108.75. Depending on your locale, the decimal point might be a comma (,) instead of a period (.).
*/




import java.text.DecimalFormat;

public class FPadder {
    public static void main(String[] args) {

    int numArgs = args.length;

    //this program requires at least two arguments on the command line
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
        double sum = 0.0;

        for (int i = 0; i < numArgs; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
        }

        //format the sum
        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        String output = myFormatter.format(sum);

        //print the sum
            System.out.println(output);
        }
    }
}