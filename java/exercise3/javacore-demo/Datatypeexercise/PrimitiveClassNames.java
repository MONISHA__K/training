package com.java.training.core.Datatypeexercise;
/*
    Problem Statement
    1.Find the ClassNames of the Primitive Datatype.
    
    Entity
    1.PrimitiveClassNames Class
    
    Work Done
    1.Getting the ClassName of a Primitive type by using the .class and getName method.
    2.Storing the int class name into a String called intClassName and getting the className of the Respective 
      datatype with .class and className method..
    3.Storing the char class name into a String called charClassName and getting the className of the Respective 
      datatype with .class and className method..
    4.Storing the double class name into a String called doubleClassName and getting the className of the Respective 
      datatype with .class and className method..
    5.Storing the float class name into a String called floatClassName and getting the className of the Respective 
      datatype with .class and className method..
*/

public class PrimitiveClassNames {
    public static void main(String[] args) {
        //ClassNames of Primitive Datatypes//
        
        String intClassName = int.class.getName();
        System.out.println("ClassName of Int : "+intClassName);
        
        String charClassName = char.class.getName();
        System.out.println("ClassName of Char : "+charClassName);
        
        String doubleClassName = double.class.getName();
        System.out.println("ClassName of double : "+doubleClassName);
        
        String floatClassName = float.class.getName();
        System.out.println("ClassName of float : "+floatClassName);
    }
}