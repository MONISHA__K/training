package com.java.training.core.javalangexercise;

/*
    Problem Statement
    1.Print the absolute path of the .class file of the current class
    2.Open the source file of the running class in Notepad++ - do NOT hardcode/specify the source file in the code; 
      find the source file using APIs
    3.demonstrate object equality using 
      Object.equals() vs ==, using String objects
      
    Entity
    1.FileDemo Class
    
    Jobs to be done:
    1.Delcare a File object as file and specify the File name as Parameter.
    2.Then get the Absolute Path of a File using getAbsolutePath Function and print it.
    
    1.Get the Current Working File without HardCoding instead find it using APIs.
    2.By using getProperty method and passing the parameter as "user.dir" to get the directory of the working File.
    
    1.Comparing the  Object equality using equals() Function and == Operator.
    2.Declare a String Object as firstout and initialize as "Yoga"
    3.Declare another String Object as secondout and initialize as "Balajee"
    4.Declare anothre String object as thirdout and initialize as "Yoga"
    5.Compare the String Objects firstout and thirdout with equals method and print True if its True.
    6.Comapare the String Objects firstout annd secondout with == operator and print Flase if its True.
*/

import java.io.*;

public class FileDemo {
    public static void main(String[] args) {
        //Getting the Absolute Path of a File//
        File file = new File("FileDemo.class");
        System.out.println(file.getAbsolutePath());
        
        //Getting the Path of a File without HardCoding//
        System.out.println(System.getProperty("user.dir"));
        
        //String Object Comparision//
        String firstout = "Yoga";
        String secondout = "Balajee";
        String thirdout = "Yoga";
        
        if(firstout.equals(thirdout)) {
            System.out.println("True");
        } else if(firstout == secondout) {
            System.out.println("False");
        }
    }
}