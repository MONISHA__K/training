package com.java.training.core.enumexercises;
/*
    Problem Statement
    compare Enum Values with 
    1.Equal Function
    2. == Operator
    
    Entity
    1.Enum Values
    
    Work Done
    1.Creating Enum Class with Default Values
    2.Get User Value
    3.Comparing the ENUM Values with == Operator
    4.Comparing the ENUM Values with equals function
    
    
*/
import java.util.Scanner;
public class EnumClass {
    public enum Days {monday,tuesday,wednesday,thursday,friday,saturday,sunday}
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the Day to get the Past Weather Forcast");
        System.out.println("monday\n"
                           +"tuesday\n"
                           +"wednesday\n"
                           +"thursday\n"
                           +"friday\n"
                           +"saturday\n"
                           +"sunday\n");
        String getday = scan.nextLine();//Get User Value//
        Days days = Days.valueOf(getday.toUpperCase());
        if(days.equals(Days.monday)) {
            System.out.println("Cloudy");
        }
        else if(days.equals(Days.tuesday)) {
            System.out.println("Weather :"+"Partial Cloudy");
        }
        else if(days.equals(Days.wednesday)) {
            System.out.println("Weather :"+"Humid");
        }
        else if(days.equals(Days.thursday)) {
            System.out.println("Weather :"+"Hot");
        }
        else if(days == Days.friday) {
            System.out.println("Weather :"+"Humid");
        }
        else if(days == Days.saturday) {
            System.out.println("Weather :"+"sundayny");
        }
        else if(days == Days.sunday) {
            System.out.println("Weather :"+"Partial Cloudy");
        }
        else {
            System.out.println("Enter a Valid One!!");
        }
        
    }
}