package com.java.training.core.operatorexercises;

/* 
In the following program, explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }
*/
/*Requirment:
    To explain why the value "6" is printed twice in a row

Entites
    PrePostDemo - Class name

Explanation:
6 is printed twice because it is preincremented.
Pre-increment means that the variable is incremented BEFORE it's evaluated in the expression whereas Post-increment means that the variable is incremented AFTER it has been evaluated for use in the expression.
*/


public class PrePostDemo {
    public static void main(String[] args) {
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        
        System.out.println(++i);  // "6"
        //Pre Increment Operator increases the value first and then assigns //
        
        System.out.println(i++);  // "6"
        //Post Increment Operator assigns the value first and then increase second//
        
        System.out.println(i);    // "7"
    }
}