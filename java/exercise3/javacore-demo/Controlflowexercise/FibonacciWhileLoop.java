package com.java.training.core.Controlflowexercise;
/*+ print fibinocci using  while loop


Requirement:
    To find fibonacci series using while loop.

Entity:
     FibonacciWhileLoop 

Function Declaration:
    Here there is no function is declared.

Jobs to be Done:
    1. Declare the class FibonacciWhileLoop.
    2. First declare the range to which the Series to be printed and assign the first two numbers of
       the series under two different variables as a and b.
    3. Then use while loop. Assign the value to the new variable i. Continue the process till the 
       condition meets a<c.
    4. Now add the values of a and b and store it in new variable sum.
    5. Now assign the value of b to a and the value of b to sum and then increments the value of i.
    6. Now print a.
*/


import java.util.Scanner;
public class FibonacciWhileLoop {
    public static void main(String args[]) {
        int a=0,b=1,temp;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number to find its fibonacci seriesn");
        int c = scan.nextInt();
        while(a<c) {
            System.out.print(a+" ");
            temp = a + b;
            a = b;
            b = temp;
        }
    }
}