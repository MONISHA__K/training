package com.java.training.core.Controlflowexercise;

/*+ print fibinocci using recursion
Requirement:
    To find fibonacci series using Recursion.

Entity:
    FibonacciUsingRecursion 

Function Declaration:
    public static void printFibonacci, is the function declared.

Jobs to be Done:
    1. Declare the class FibonacciUsingRecursion.
    2. Declare and assign the first two variable as 1 and 0 and declare the third variable temp.
    3. Now declare the function as static void Fibonacci(int c) and give the body of
       the function.
    4. Call the function Fibonacci(int c) by declaring the variable c.
    5. Now print the series using print statement.
*/



import java.util.Scanner;

public class FibonacciRecursion {
    static int a=0;
    static int b=1;
    static int temp;
    static void fibonacci(int c) {
        if(c>0) {
            System.out.print(a+" ");
            temp = a + b;
            a = b;
            b = temp;
            fibonacci(c-1);
        }
    }
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number to find its fibonacci seriesn");
        int c = scan.nextInt();
        fibonacci(c);
    }
}