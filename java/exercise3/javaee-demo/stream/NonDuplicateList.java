package com.java.training.stream;

/*
Requirement:
    Consider a following code snippet:
        List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
    - Get the non-duplicate values from the above list using java.util.Stream API

Entity:
    NonDulicateList

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an reference forrandoNumbers and convert the list to an array.
    2. Now create another object for the list to store the stream of collections of numbers which are not
       repeated
          2.1) First filter the elements of randomNumbers in which the elements are present only once.
          2.2) If it present only once then store it in withoutDuplicate
          2.3) Continue this process until the last element in the randomNumber.
    3. Print those elements present in withoutDuplicate
Pseudo code:
class NonDuplicateList {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        List<Integer> withoutDuplicate = randomNumbers.stream()
                .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                .collect(Collectors.toList());
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements));
    }
}*/

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicateList {
    
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        List<Integer> withoutDuplicate = randomNumbers.stream()
                .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                .collect(Collectors.toList());
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));
    }
}