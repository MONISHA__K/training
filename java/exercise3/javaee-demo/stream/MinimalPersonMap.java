package com.java.training.stream;

/*
Requirement:
    To print minimal person with name and email address from the Person class using java.util.Stream<T>#map API by referring Person.java
  
Entity:
  	MinimalPersonMap
 
Function Declaration:
 	public static <R, T> void main(String[] args)
 
Jobs To Be Done:
  	1) Accessing the Predefined roster list as a name roster.
  	2) Creating the arraylist as name and mail id and adding the values from the roster list through Steam mapping.
  	3) Finding the minimal of name
  	4) Printing the Minimal Person name and mailId. 
  
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class MinimalPersonMap {

	public static <R, T> void main(String[] args) {
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = (ArrayList<String>) roster.stream().map(s -> s.getName())
				.collect(Collectors.toList());
		
		ArrayList<String> mailId = (ArrayList<String>) roster.stream().map(s -> s.getEmailAddress())
				.collect(Collectors.toList());
		
		String minimalName = Collections.min(name);
		String minimalId = Collections.min(mailId);

		System.out.println(
				"The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
	}
}
