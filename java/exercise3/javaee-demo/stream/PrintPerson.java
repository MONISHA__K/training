package com.java.training.stream;

/*
Requirement:
    Print all the persons in the roster using java.util.Stream<T>#forEach 

Entity:
    public class PrintPerson

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an reference for the list of type person.
    2. Now Create an Stream for an person
    3. For each element in the stream print the result for given.
Pseudo code:
class PrintPerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}
*/

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class PrintPerson {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> System.out.println(person.name + " " +person.birthday + " " +person.gender + " " + person.emailAddress));
	}
}
