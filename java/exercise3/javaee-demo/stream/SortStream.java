package com.java.training.stream;

/*
Requirement:
    Sort the roster list based on the person's age in descending order using java.util.Stream

Entity:
    SortStream
      
Function declaration:
    public static void main(String[] args)
 
Jobs to be done:
   1. Create an object newRoster to store the elements of roster list based on descending order based on age
   2. print each element in the stream. 
Pseudo code:
class SortStream {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = roster.stream().sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());
        Stream<Person> stream = newRoster.stream();
        //roster.sorted(Comparator.reverseOrder());
        stream.forEach(person -> 
            System.out.println(person.getAge() + " " + person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
    
}*/

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortStream {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = roster.stream().sorted(Comparator.comparing(Person::getAge)).collect(Collectors.toList());
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}