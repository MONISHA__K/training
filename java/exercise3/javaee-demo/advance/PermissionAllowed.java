/*
 * Requirements : 
 * 		Get the permission allowed for a file.
 *
 * Entities :
 * 		PermissionAllowed
 * Method Signature :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Create a reference for Paths.
 * 		2)Print whether the file is executable.
 * 		3)Print whether the file is hidden.
 *		4)Print whether the file is readable.
 * 		5)Print whether the file is writable.
 *
 * PseudoCode:
 * 		class PermissionAllowed {
 *
 *			public static void main(String[] args) throws IOException {
 *				Path path = Paths.get("ReaderEx.txt");
 *				System.out.println("Is Executable : " + Files.isExecutable(path));
 *				System.out.println("Is Hidden : " + Files.isHidden(path));
 *				System.out.println("Is Readable : " + Files.isReadable(path));
 *				System.out.println("Is Writable : " + Files.isWritable(path));
 *			}
 *		
 *		}
 */
package com.java.training.advance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PermissionAllowed {

	public static void main(String[] args) throws IOException {
		Path path = Paths.get("ReaderEx.txt");
		System.out.println("Is Executable : " + Files.isExecutable(path));
		System.out.println("Is Hidden : " + Files.isHidden(path));
		System.out.println("Is Readable : " + Files.isReadable(path));
		System.out.println("Is Writable : " + Files.isWritable(path));
	}

}
