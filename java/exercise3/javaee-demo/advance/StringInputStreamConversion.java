/*
 * Requirements:
 *     To convert the InputStream to String and vice versa.
 *    
 * Entities:
 *     StringInputStreamConversion
 *     
 * Method Signature:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Create a reference for FileInputStream with file as constructor argument.
 *     2.Get the content of the file and convert to string
 *     3.Print the string
 *     3.Convert the given string into input stream 
 *     4.Write the converted content to a file
 *     5.Close the created output stream.
 *
 * Pseudo code:
 *     class StringInputStreamConversion {
 *
 *         public static void main(String args[]) throws IOException {
 *		       InputStream inputStream = new FileInputStream("outputFile.txt");
 *		       Scanner scanner = new Scanner(inputStream);
 *		       StringBuffer stringBuffer = new StringBuffer();
 *		       while(scanner.hasNext()){
 *		          stringBuffer.append(scanner.nextLine());
 *		       }
 *		       System.out.println(stringBuffer.toString());
 *		      
 *		       String content = "The Java OutputStream class, java.io.OutputStream, is the" +
 *				         " base class of all output streams in the Java IO API. Subclasses" + 
 *				         " of OutputStream include the Java BufferedOutputStream and the " +
 *				         "Java FileOutputStream among others. To see a full list of output" +
 *				         " streams, go to the bottom table of the Java IO Overview page." ;
 *		       InputStream stringInputStream = new ByteArrayInputStream(content.getBytes());
 *		       int data;
 *		       while((data = stringInputStream.read()) != -1) {
 *		           System.out.print((char) data);
 *		           data = inputStream.read();
 *		       }
 *		   }
 *     }
 */

package com.java.training.advance;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class StringInputStreamConversion {
	
   public static void main(String args[]) throws IOException {
       InputStream inputStream = new FileInputStream("outputFile.txt");
       Scanner scanner = new Scanner(inputStream);
       StringBuffer stringBuffer = new StringBuffer();
       while(scanner.hasNext()){
          stringBuffer.append(scanner.nextLine());
       }
       scanner.close();
       System.out.println(stringBuffer.toString());
       inputStream.close();
      
       String content = "The Java OutputStream class, java.io.OutputStream, is the " +
		                "base class of all output streams in the Java IO API. Subclasses " + 
		                "of OutputStream include the Java BufferedOutputStream and the " +
		                "Java FileOutputStream among others. To see a full list of output " +
		                "streams, go to the bottom table of the Java IO Overview page." ;
       InputStream stringInputStream = new ByteArrayInputStream(content.getBytes());
       int data;
       while((data = stringInputStream.read()) != -1) {
           System.out.print((char) data);
       }
       stringInputStream.close();
   }
}