/*
Requirement:
    To find the difference between the times using timer class.
    
Entity:
    TimeDifferenceDemo

Function Declaration:
    public static void main(String[] args)
    public static void countFunction(long time)
    
Jobs to be done:
    1. Declare the long variable startTime which gets the System time in milliseconds.
           1.1) Print that starting time.
    2. for each milliseconds starting from 0 ms ending up to 100 ms 
           2.1) print that milliseconds.
    3. Now get the current system timing in millisecond and store it in the endTime.
    4. Print the difference between startTime and endTime.
    
Pseudo code:

class TimeDifferenceDemo {
    
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        System.out.println("the starting time is: " + startTime + " " + "ms");
        
        for (int time = 0; time < 100 ; time++) {
            System.out.println(time);
        }
        
        long endTime = System.currentTimeMillis();
        System.out.println("End time in ms is: " + end + " " + "ms");
        System.out.println("THe difference between the time is: " + (startTime - endTime) + "  " + "ms");
    }
}
*/
package com.java.training.date_and_time;

public class TimeDifferenceDemo {
    
    public static void main(String[] args) {
        long start = System.currentTimeMillis(); 
        System.out.println(  "staring time in millsecond  :  " + start + "ms");
            
        for (long time = 0; time < 100; time++) {
            System.out.println(time);
        }
      
        long end = System.currentTimeMillis(); 
        System.out.println("End  time in millsecond :  " + end + "ms");
        System.out.println("Counting to 100 takes " + (end - start) + "ms"); 
    } 
}
