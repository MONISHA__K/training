/* 
Requirement:
    To code for the schedule() method in Timer class.

Entity:
    ScheduleMethodDemo

Function Declaration:
    public static void main(String[] args)
    public void run()
    
Jobs to be done:
    1. Reference is created for a Timer class and also the reference is created for TimerTask.
    2. For each time that is from 0 to 10 
           2.1) Print that time and cancel that timer.
           2.2) Now remove the cancelled timer and print the removed value.
    3. Now schedule the task whose execution at specified time(1000 ms) for repeated delay for 1 ms.
    
Pseudo code:
public class ScheduleMethodDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int time = 0; time <= 10; time++ ) {
                    System.out.println( "number : " + time);
                    timer.cancel();
                    System.out.println("stop" + " purge value of Task : " + timer.purge());
                }   
            }
        };
        
        timer.schedule(task,1000,1);
    }
}

*/
package com.java.training.date_and_time;

import java.util.Timer;
import java.util.TimerTask;

public class ScheduleMethodDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int time = 0; time <= 10; time++ ) {
                    System.out.println( "number : " + time);
                    timer.cancel();
                    System.out.println("stop" + " purge value of Task : " + timer.purge());
                }   
            }
        };
        
        timer.schedule(task,1000,1);
    }
}
        
  
