package com.java.training.core.lambda;

/*
Requirement:
    To print the sum of the variable arguments.

Entity:
    public class LambdaVariableArguments

Function declaration:
    public int printAddition(int ... numbers)
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Create a interface VariableArgumentDemo.
    3. Declare the class LambdaVariableArguments as public.
    4. Declare a method public int printAddition(int ... numbers)
    5. Under a main method Create a lambda expression and call the method and print the result.  
 Pseudo code:
class VariableArgumentDemo {
    
    public static void main(String[] args) {
        VariableArgument value = (numbers) -> {
            int sum = 0;
            
            for (int values : numbers) {
                sum = sum + values;
            }
            
            return sum;
        };
        
        System.out.println(value.printAddition(value1, value2, value3));
    }
}

*/
interface VariableArgumentDemo {
    
    public int printAddition(int ... numbers);
}

public class LambdaVariableArguments {
    
    public static void main(String[] args) {
        VariableArgumentDemo value = (numbers) -> {
            int sum = 0;
            
            for (int values : numbers) {
                sum = sum + values;
            }
            
            return sum;
        };
        
        System.out.println("The sum of elements is: " + value.printAddition(1, 5, 6, 7));
    }
}
