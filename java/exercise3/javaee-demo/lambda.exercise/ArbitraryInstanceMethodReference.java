package com.java.training.core.lambda;

/*
Requirement:
    To explain about the MethodInterface and its types.

Entity:
    public classArbitraryInstanceMethodReference

Function Declaration:
       public void printName()
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Declare the class ArbitraryInstanceMethodReference as public.
    3. import the List and array package.
    4. Create an object for list of type Person and initialize it and convert it to Array.
    5. It is an example for ArbitraryInstanceMethodReference.
    6. Under a main method Create a lambda expression and call the method and print the result.
    7. Create a static class Person and print the name of the person.     
 */
import java.util.Arrays;
import java.util.List;

public class ArbitraryInstanceMethodReference {

    public static void main(String args[]) {
        final List<Person> people = Arrays.asList(new Person("dan"), new Person("laura"));
        // Method reference
        people.forEach(Person::printName);
        // Lambda expression
        people.forEach(person -> person.printName());

        // normal
        for (final Person person : people) {
            person.printName();
        }
    }

    private static class Person {

        private String name;

        public Person(final String name) {
            this.name = name;
        }

        public void printName() {
            System.out.println(name);
        }
    }
}

/*
This calls the method Person.getName for each Person object in the list. Person is the particular
type, and the arbitrary object is the instance of Person that is used during each loop.
*/