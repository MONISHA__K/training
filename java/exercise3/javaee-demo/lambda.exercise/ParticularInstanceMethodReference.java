package com.java.training.core.lambda;

/*
Requirement:
    To explain about the MethodInterface and its types.

Entity:
    public class ParticularInstanceMethodReference

Function Declaration:
    public int compare(final Integer a, final Integer b)
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Declare the class MethodReferenceDemo as public.
    3. import the List, Collection and array package.
    4. Create an object for list of type Integer and initialize it and convert it to Array.
    5. It is an example for ParticularInstanceMethodReference 
    6. Under a main method Create a lambda expression and call the method and print the result.     
*/

import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public class ParticularInstanceMethodReference {

    public static void main(String args[]) {
        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final MyComparator myComparator = new MyComparator();
        // Method reference
        Collections.sort(list, myComparator::compare);
        // Lambda expression
        Collections.sort(list, (a, b) -> myComparator.compare(a, b));
        System.out.println(list);
    }

    private static class MyComparator {

        public int compare(final Integer a, final Integer b) {
            return a.compareTo(b);
        }
    }
    
 
}

/*
Here, it calls the instance method myComparator.compare, where myComparator is a particular instance of MyComparator.
*/
