package com.java.training.core.lambda;

/*
Requirement:
    To code the simple lambda expression using the method getValue()

Entity:
    public class SimpleLambdaDemo 

Function Declaration:
    public int getValue()
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Create a interface LambdaDemo.
    3. Declare the class SimpleLambdaDemo as public.
    4. Declare a method public int getValue()
    5. Under a main method Create a lambda expression and call the method and print the result.     
Pseudo code:
class LambdaDemo {
    
    public static void main(String[] args) {
        SampleDemo value = () -> value;
        System.out.println(value.getValue());
    }
}

*/

interface LambdaDemo {
    
    public int getValue();
}

public class SimpleLambdaDemo {
    
    public static void main(String[] args) {
        LambdaDemo value = () -> 555;
        System.out.println("The value is: " + value.getValue());
    }
}
