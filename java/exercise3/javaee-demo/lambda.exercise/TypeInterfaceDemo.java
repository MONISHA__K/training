package com.java.training.core.lambda;

/*
Requirement:
    To find the error line and to fix it using BiFunction Interface for the program.
    public interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);
        
        System.out.println(print);
    }
}

Entity:
    public class TypeInterfaceDemo
    
Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Declare class TypeInterfaceDemo as public.
    3. There is no need of declaring the seperate interface we can import the BiFunction interface
       from util.function package.
    4. The line int print = function.print(int 23, int 32) is error.
    5. Fix it by using BiFunction interface  
Pseudo code:
class TypeInterfaceDemo {
    
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
            return number1 + number2;
        };        
        
        System.out.println(function.print(number1, number2));
    }

}
*/

import java.util.function.BiFunction;

/*interface BiFunction {
    int print(int number1, int number2);
}*/

public class TypeInterfaceDemo {
    
    public static void main(String[] args) {

        BiFunction<Integer, Integer, Integer> function = (number1, number2) ->  { 
            return number1 + number2;
        };        
        
        System.out.println(function.apply(23, 32));
    }

}
