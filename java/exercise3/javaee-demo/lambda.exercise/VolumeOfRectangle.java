package com.java.training.core.lambda;

/* 
Requirement:
    To print the volume of the rectangle using Lambda Expressions.
    
Entity:
    public class VolumeOfrectangle.

Function Declaration:
    public double printVolume(double length, double width, double height)
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Create a interface Rectangle.
    3. Declare the class VolumeOfRectangle as public.
    4. Declare a method  public double printVolume(double length, double width, double height)
    5. Under a main method Create a lambda expression and call the method and print the result.     
Pseudo code:
class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        System.out.println(volume.printVolume(length, width, height));
    }
}
*/

interface Rectangle {
    
    public double printVolume(double length, double width, double height);
}

public class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        
        System.out.println("The volume of the rectangle is: " + volume.printVolume(12.44, 667.434, 66.987));
    }
}
