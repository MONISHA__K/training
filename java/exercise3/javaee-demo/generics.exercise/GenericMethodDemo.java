package com.java.training.core.generics;

/*
Requirements : 
    Write a generic method to count the number of elements in a collection that have a specific
    property (for example, odd integers, prime numbers, palindromes).

Entities :
    public class CountSpecificProperty.
  
Function Declaration :
    public static void main(String[] args)
  
Jobs To Be Done:
    1) Creating the package com.java.training.core.generics
    2) Importing the ArrayList.
    2) Creating the GenericMethodDemo class as public
    3) Creating the count method which returns the count of odd numbers present in a list.
    4) Creating the main method and create a list reference.
    5) Adding elements inside a list.
    6) Calling a count method and printing the number of odd numbers.
Pseudo code:
class GenericsMethodDemo {

    public static int countOddNumber(ArrayList<Integer> list) {
        int value = 0;
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }

        return value;
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(elements);
        System.out.println(countOddNumber(list));
    }
}
*/

import java.util.ArrayList;

public class GenericMethodDemo {
    
    public static  int countOddNumbers(ArrayList<Integer> list) {
        int value = 0;
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }
        
        return value;
    }
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(25);
        list.add(15);
        list.add(22);
        list.add(24);
        list.add(26);
        list.add(27);
        list.add(21);
        list.add(20);
        System.out.println("the count of odd number is " + countOddNumbers(list));
    }
}
