/*
Requirement:
    To write a program to demonstrate generics - for loop, for list, set and map

Entity:
    public class GenericForLoopDemo
    
Function declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a hash set instance .
        1.1 Add string type elements to the set.
        1.2 Print the elements.
    2. Create a array list instance.
        2.1 Add integer type elements to it.
        2.2 Print the elements.
    3. Create a hash map instance.
        3.1 Add integer as key and string as value.
        3.2 Print the hash map.
        
Pseudo code:
class GenericForLoopDemo {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add(elements);
        for (String string : set) {
            System.out.println(string);
        }

        ArrayList<Integer> list = new ArrayList<>();
        list.add(elements);
        for (int numbers : list) {
            System.out.println(numbers);
        }

        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(key, value);
        for (Integer key : hashMap.keySet()) {
            String value = hashMap.get(key);
            System.out.println(key + ":" + value);
        }

   
    }
}
*/
package com.java.training.core.generics;

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;

public class GenericForLoopDemo {
    
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Zero");
        set.add("Jackiechan");
        set.add("Robert");
        for (String string : set) {
            System.out.println("The elements are " + string);
        }
        
        ArrayList<Integer> list = new ArrayList<>();
        list.add(125);
        list.add(150);
        list.add(175);
        for (int numbers : list) {
            System.out.println("the elements of the list are " + numbers);
        }
        
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Robert");
        hashMap.put(2, "Downey");
        hashMap.put(3, "Suresh");
        for (Integer key : hashMap.keySet()) {
            String value = hashMap.get(key);
            System.out.println(key + ":" + value);
        }
        
        for (String value : hashMap.values()) {
            System.out.println("The values are" + value);
        }
    }
}
