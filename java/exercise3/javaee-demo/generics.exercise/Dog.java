package com.java.training.core.generics;

/*
Requirements : 
    To write a program to demonstrate generics - class objects as type literals.
 
Entities :
    public class Dog implements Animal
     
Function Declaration :
    public static <T> boolean checkInterface(Class<?> theClass)
    public void sound()
    public static void main(String[] args)
     
Jobs To Be Done:
    1) Creating the class Dog and implementing the interface Animal
    2) Defining the methods checkInterface and sound
    3) In main method create the references for the class Class with different types
    4) Invoke the method checkInterface with variable intClass and with the class references
    5) print the respective results
    6) Creating the class reference with forName method and invoke the method checkInterface.   
Pseudo code:
interface Animal {
    public void sound();
}


class Dog implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Barking");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class; // String.class,Integer.class,Boolean.class
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1); // prints false
        System.out.println(intClass.getClass()); // prints java.lang.Class
        System.out.println(intClass.getName()); // prints int

        boolean boolean2 = checkInterface(Dog.class);
        System.out.println(boolean2); // prints false
        System.out.println(Dog.class.getClass()); // prints java.lang.Class
        System.out.println(Dog.class.getName()); // prints Dog

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3); // prints true
        System.out.println(Animal.class.getClass()); // prints java.lang.Class
        System.out.println(Animal.class.getName()); // prints Animal

        
    }
}
*/
interface Animal {
    public void sound();
}

public class Dog implements Animal {
    
    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Barking");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            //String.class,Integer.class,Boolean.class
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   //prints false
        System.out.println(intClass.getClass());        //prints java.lang.Class
        System.out.println(intClass.getName());         //prints int

        boolean boolean2 = checkInterface(Dog.class);
        System.out.println(boolean2);                   //prints false
        System.out.println(Dog.class.getClass());       //prints java.lang.Class
        System.out.println(Dog.class.getName());        //prints Dog

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3);                   //prints true
        System.out.println(Animal.class.getClass());    //prints java.lang.Class
        System.out.println(Animal.class.getName());     //prints Animal

        try {
            Class<?> testClass = Class.forName("Dog");
            System.out.println(testClass.getClass());
            System.out.println(testClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
}