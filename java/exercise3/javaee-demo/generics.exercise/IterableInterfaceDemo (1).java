package com.java.training.core.generics;

/*
Requirement:
    To  Write a program to print employees name list by implementing iterable interface.

Entity:
    class Employee.
    class EmployeeList.
    public class IterableInterfaceDemo.

Function Declaration:
    public String getFirstName()
    public void setFirstName(String firstName)
    public String getlastName()
    public void setlastName(String lastName)
    public int getEmployeeId() 
    public void setEmployeeId(int employeeId)
    public String toString() 
    public void addEmployee(Employee employee)
    public void removeEmployee(Employee employee)
    public static void main(String[] args)
    public int employeesListSize()
    public Iterator<Employee> iterator()
    
Jobs to be done:
    1. Create a package com.java.training.core.generics.
    2. Import lang and util packages.
    3. Create a class Employee.
    4. Declare the variables firstName, lasttName of typeString and employeeId of type
       integer.
    5. Create a constructor and read the values.
    6. Using getters and setters get and set firstName, lastName and employeeId.
    7. Return the result by converting to String by using toString() method.
    8. Now create a class EmployeeList of type Employee by implementing from Iterable.
    9. Create a constructor and store the values as list.
    10. Now add and remove the employee using addEmployee() method removeEmployee method().
    11. Create a class IterableInterfaceDemo as public.
    12. Under a main method Create an object for Employee as employee1 and employee2.
    13. Create an object for EmployeeList and add the values from employee1 and employee2.
    14. Using for each loop print the list. 
Pseudo code:
class IterableDemo implements Iterable<String> {

    public ArrayList<String> employeeList;

    public IterableDemo() {
        employeeList = new ArrayList<String>();
    }

    public void addEmployee(String employee) {
        employeeList.add(employee);
    }

    public Iterator<String> iterator() {
        return employeeList.iterator();
    }

    public void checkMethod() {
        employeeList.forEach((member) -> {
            System.out.println(member);
        });
    }

    public static void main(String[] args) {
        IterableDemo employees = new IterableDemo();
        employees.addEmployee(names);
        employees.checkMethod();
        for (String name : employees) {
            System.out.println(name);
        }
    }
}

*/

import static java.lang.String.format;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class Employee {

    private String firstName;
    private String lastName;
    private int employeeId;

    public Employee(String firstName, String lastName, int employeeId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
    
    @Override
    public String toString() {
        return format("First Name: %s Last Name: %s EmployeeId: %d", firstName, lastName, employeeId);
    }
}

class EmployeeList implements Iterable<Employee> {
    private List<Employee> employees;
    
    public EmployeeList() {
        employees = new ArrayList<Employee>();
    }
    
    public EmployeeList(int employeeId) {
        employees = new ArrayList<Employee>(employeeId);
    }
    
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
    
    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }
    
    public int employeesListSize() {
        return employees.size();
    }

    @Override
    public Iterator<Employee> iterator() {
        // TODO Auto-generated method stub
        return employees.iterator();
    }
}

public class IterableInterfaceDemo {
    public static void main(String[] args) {
        Employee employee1 = new Employee("John", "Vijay", 450);
        Employee employee2 = new Employee("Joseph", "Surya", 675);
        EmployeeList employeeList = new EmployeeList();
        employeeList.addEmployee(employee1);
        employeeList.addEmployee(employee2);
        for (Employee employee : employeeList) {
            System.out.println(employee);
        }
    }
}
