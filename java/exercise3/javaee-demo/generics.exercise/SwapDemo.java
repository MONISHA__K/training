package com.java.training.core.generics;

/*
Requirement:
    To write a generic Method to swap the elements.

Entity:
    public class SwapDemo

Function Declaration:
    public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    1. Create package com.java.training.core.generics.
    2. Import packages Arrays, Collections, List, ArrayList.
    3. Create a class SwapDemo
    4. Create a method swap() of type T and swap the elements using temperary variables.
    5. Create another method printSwap() and swap by using Collections.swap().
    6. Under a main method create an array and call the swap method.
    7. print the result.
    8. Create an object for List of type Integer and call printSwap() method
    9. Print the result.
Pseudo code:
class SwapDemo {

    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temporaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temporaryVariable;
        System.out.println(Arrays.toString(list));
    }

    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T>swap(list1, firstNumber, secondNumber);
    }

    public static void main(String[] args) {
        Integer[] list = {elements};
        swap(list, first number, last number);

        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, first number, last number);
        System.out.println(list1);
    }
}
*/

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class SwapDemo {
    
    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }
    
    @SuppressWarnings("unused")
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T> swap(list1, firstNumber, secondNumber);
    }
    
    public static void main(String[] args) {
        Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 2, 4);
        System.out.println(list1);
        
    }
}

