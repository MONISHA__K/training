package com.java.training.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.*;

/*
 * 1)Requirements
 * 		Write a Java program to copy all of the mappings from the specified map to another map
 * 
 * 2)Entity
 * 		MapToMap Class
 * 
 * 3)Jobs to be done
 * 	1.Created a new HashMap called oldMap and added some values to it
 * 	2.Now created mapObj Object for the Class MapToMap.
 * 	3.Called mapConverter private method which has the Map as a Parameter.
 * 	4.In the mapConverter method it creates a new HashMap and the HashMap passed as parameter is added to the 
 *   newMap by using putAll method provided.
 * 	5.Now the newMap HashMap is printed to the console by using the entrySet method and printed the key and value.
 * 
 * 4)Pseudo code
 *	public class MapToMap {
	
	private void mapConverter(Map<Integer, String> oldMap)
	
	//Another HashMap to store all the new HashMap values.
			Map<Integer,String> newMap = new HashMap<Integer,String>();
	
	public static void main(String[] args) {
		Map<Integer,String> oldMap = new HashMap<Integer,String>();
		
		MapToMap mapObj = new MapToMap();
		
		//mapConverter method is called.
		mapObj.mapConverter(oldMap);
 *
 */
public class MapToMap {
	
	private void mapConverter(Map<Integer, String> oldMap) {
			
			//Another HashMap to store all the new HashMap values.
			Map<Integer,String> newMap = new HashMap<Integer,String>();
			
			//OldMap values are added to the newMap.
			newMap.putAll(oldMap);
			
			for(Map.Entry<Integer, String> mapValues : newMap.entrySet()) {
				System.out.println(mapValues.getKey()+" "+mapValues.getValue());
			}
	}
	
	public static void main(String[] args) {
		
		//New HashMap
		Map<Integer,String> oldMap = new HashMap<Integer,String>();
		
		//Values Added.
		oldMap.put(1,"Yoga Balajee");
		oldMap.put(2,"Yogi");
		oldMap.put(3,"Lithi");
		
		System.out.println(oldMap);
		
		//MapToMap Class Object
		MapToMap mapObj = new MapToMap();
		
		//mapConverter method is called.
		mapObj.mapConverter(oldMap);
		
	}
	
	

}
