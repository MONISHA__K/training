package com.java.training.regex;

/*REQUIRMENT:
 *  To write a jwrite a program to display  escape sequence in java
		She walks in beauty, like the night, 
        Of cloudless climes and starry skies 
        And all that's best of dark and bright 
        Meet in her aspect and her eyes�

   ENTITY:
      EscapeSequenceB
      
   JOBS TO BE DONE:
   
       Declare and name a class .
       Initialize the given string to a new variable and use the respective escape sequence to get in the given format.
       then print the string.
       
    PSEUDO CODE:
      public class EscapeSequenceA{
public static void main(String[] args) {
    String myFavoriteBook = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...")
       */



public class EscapeSequenceB {
	   public static void main(String[] args) {
	       String byron = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
	       System.out.println(byron);
	   }
	}