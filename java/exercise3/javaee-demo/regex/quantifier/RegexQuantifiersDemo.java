package com.java.training.regex.quantifier;
/*
Requirement:
    1)To write a code for java regex quantifiers
    
Entity:
    1)RegexQuantifiers

Function declaration:
    1)public static void main(String[] args)

Jobs to be done:
    1. Create a String variable text and assign the value 
    2. Use the quantifiers and print the result.
    
Pseudo code:
class RegexQuantifiers {
    
    public static void main(String[] args) {
        String text = "MSMSMSMSMSSSSMMMS";
        Pattern pattern = Pattern.compile("M?S);
        Matcher matcher = pattern.matcher(text);
        int i = 0;
        while (matcher.find()) {
            i++;
    // print the result;
    }
}

*/


import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegexQuantifiers {
    
    public static void main(String[] args) {
        String text = "ybybybybybybybyb";
        Pattern pattern = Pattern.compile("y?b");
        Matcher matcher = pattern.matcher(text);
        
        int i = 0;
        while (matcher.find()) {
            i++;
            System.out.println(i + matcher.group());
        }
        
    }
}