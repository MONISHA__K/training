package com.java.training.listset;
/* 
 Create a list
    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
+ Explain about contains(), subList(), retainAll() and give example
 
 ----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------

1.Requirements
   To perform tasks in list such as adding elemts,finding index,printing values and so on.
2.Entities
   - SetDemo
3.Function Declaration

4.Jobs to be done
   1.First Create a class and declare main.
   2.Inside the main create a list called topics and add 10 topics using add() method and printing the added topics list.
   3.Creating another list of newTopics and adding 4 newTopics using addAll() method adding topics and new java core topics.
   4.Finding the index of given topic using indexOf() and lastIndexOf().
   5.Print the values in the list using
             1.In For loop getting index value and printing the value
             2.In Foreach printing the value assigning to variable topic
             3.In While loop with hasNext printing the value.
             4.Stream API using :: method reference printing the value.
   6.Converting the list into array and set.
   7.Checking the set with contain() method to set containing or not.
   8.Creating new list adding values from already created list topics using sublist(stratIndex, lastIndex) method.
   9.Finding the added values newly created list and topics list using retainAll() method.

5.pseudo code
	public class ListDemo {
	public static void main(String[] args) {
		
	//Create a list
	List<String> topics = new ArrayList<String>();
	//Add 10 values in the list
	topics.add
	System.out.println(""+topics);
	
	//Create another list and perform addAll() method with it
	List<String> newTopics = new ArrayList<String>();
	newTopics.add
	
	//Find the index of some value with indexOf() and lastIndexOf()
	System.out.println(" " + topics.indexOf("Classes"));
	System.out.println(" " + topics.lastIndexOf("Variables"));
	
	//Print the values in the list using 
	//Using For loop
    for (int index = 0; index < topics.size(); index++) {
        System.out.println(topics.get(index));
        
 	//Using For each
    for (String topic : topics) {
        System.out.println(topic);
        
    //Using While loop
    Iterator<String> topicsIterator = topics.iterator();
    while(topicsIterator.hasNext()) {
        System.out.println(topicsIterator.next());
        
     //Using Stream API's forEach
    System.out.println(" ");
    topics.forEach(System.out::println);

    //Convert the list to a set
    Set<String> topicsSet = new HashSet<>(topics);
 
    //Convert the list to a array
    String[] topicsArray = new String[topics.size()];
    
    //contains()
    System.out.println(topics.contains("Method Reference"));

    //subList(startIndex, lastIndex)
    List<String> newAddedTopics = topics.subList(10, topics.size());

    //retainAll()
    topics.retainAll(newAddedTopics) + "\n" + topics);
-----------------------------------------------Program--------------------------------------------------------------------------------*/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListDemo {
	public static void main(String[] args) {
		
	//Create a list
	List<String> topics = new ArrayList<String>();
	//Add 10 values in the list
	topics.add("Primtive Dataypes");
	topics.add("Literals");
	topics.add("Variables");
	topics.add("Operators");
	topics.add("Control Statements");
	topics.add("Blocks of code");
	topics.add("Jump Statements");
	topics.add("Classes");
	topics.add("Methods Overiding");
	topics.add("Exceptions");
	
	System.out.println("Java Core Topics:\n"+topics);
	
	//Create another list and perform addAll() method with it
	List<String> newTopics = new ArrayList<String>();
	newTopics.add("String Comparison");
	newTopics.add("StringBuffer");
	newTopics.add("Map Interfaces");
	topics.addAll(newTopics);
	System.out.println("Java Core Topics:\n"+topics);
	
	//Find the index of some value with indexOf() and lastIndexOf()
	System.out.println("Index of JavaCoreTopic Classes \t" + topics.indexOf("Classes"));
	System.out.println("Last index of JavaCoreTopic Variables\t" + topics.lastIndexOf("Variables"));
	    
	//Print the values in the list using 
	//Using For loop
	System.out.println("----------------For loop------------------------");
    for (int index = 0; index < topics.size(); index++) {
        System.out.println(topics.get(index));
    }

    //Using For each
    System.out.println("------------------Foreach------------------------");
    for (String topic : topics) {
        System.out.println(topic);
    }

    //Using While loop
    System.out.println("------------------while loop---------------------");
    Iterator<String> topicsIterator = topics.iterator();
    while(topicsIterator.hasNext()) {
        System.out.println(topicsIterator.next());
    }

    //Using Stream API's forEach
    System.out.println("--------------------Stream API-------------------");
    topics.forEach(System.out::println);

    //Convert the list to a set
    Set<String> topicsSet = new HashSet<>(topics);
    System.out.println(topicsSet);

    //Convert the list to a array
    String[] topicsArray = new String[topics.size()];
    System.out.println(topicsArray);
    
    //contains()
    System.out.println(topics.contains("Method Reference"));

    //subList(startIndex, lastIndex)
    List<String> newAddedTopics = topics.subList(10, topics.size());
    System.out.println(newAddedTopics);

    //retainAll()
    System.out.println("After using retainAll method in topics list: " + 
    topics.retainAll(newAddedTopics) + "\n" + topics);
	}
}
