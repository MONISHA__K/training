/*
Requirement:
    To create a stack using generic type and implement
      1)Push atleast 5 elements.
      2)Pop the peek element.
      3)Search a element in stack and print the index value.
      4)print the size of stack.
      5)print the elements using stream.      
Entity:
    StackGeneric

Function Declaration:
    No function is declared

Jobs To Be Done:
    1. Create a stack of integer type and reference it as test.
    2. Add several integers in that stack.
    3. Print the output for the methods like peek,pop,indexof,size.
       3.1 Print the stack using stream. 

Pseudo code:
class StackGeneric {
    public static void main(String[] args) {
        Stack<type> test = new Stack<>();
        // push elements in stack
        test.push(elements);
        print peek element;
        print popped element;
        print test;
        print indexOf(element);
        print size();
        print test;// using stream
    }
}
*/
package com.java.training.stackandqueue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackGeneric {
    public static void main(String[] args) {
        Stack<Integer> test = new Stack<>();
        test.push(12);
        test.push(5);
        test.push(8);
        test.push(03);
        test.push(37);
        System.out.println("The peek element is " + test.peek());
        System.out.println("The poped element is " + test.pop());
        System.out.println("The Stack after poped is " + test);
        System.out.println("The Index of the element '8' is " + test.indexOf(8));
        System.out.println("The Size of the Stack is " + test.size());
        Stream<Integer> stream = test.stream();
        System.out.print("The Stack elements printed using Stream ");
        stream.forEach(i -> System.out.print(i + " "));
    }
}
