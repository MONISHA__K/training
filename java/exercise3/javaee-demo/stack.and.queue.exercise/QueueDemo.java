/*
Requirement:
    To Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
    1. add atleast 5 elements
    2. remove the front element
    3. search a element in stack using contains key word and print boolean value value
    4. print the size of stack
    5. print the elements using Stream 

Entity
    class QueueDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a linked list.
        1.1 Add elements to the list.
        1.2 Perform remove and contains method.
        1.3 Print the list using stream.
    2. Create a priority queue.
        2.1 Add elements to the list.
        2.2 Perform remove and contains method.
        2.3 Print the list using stream.

Pseudo code:
class QueueDemo {
    public static void main(String[] args) {
        Queue<type> string = new LinkedList<>();
        // add elements
        string.add(elements);
        //print list
        string.remove()//print remove element
        string.contains(element)//check for the element
        string.size// print size of list
        print list//using stream
        
        Queue<type> integer = new PriorityQueue<>();
        //add elements
        integer.add(elements);
        //print queue
        integer.remove()//print remove element
        integer.contains(element)//check for the element
        integer.size// print size of queue
        print queue//using stream
    }
}
*/
package com.java.training.stackandqueue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueDemo {

    public static void main(String[] args) {
        Queue<String> string = new LinkedList<>();
        string.add("Bike");
        string.add("Car");
        string.add("Airlines");
        string.add("Raillines");
        string.add("Lorry");
        System.out.println("The list elements are" + string);
        System.out.println("The removerd element is" + string.remove());
        System.out.println("the queue after removal of first element is" + string);
        // searching using contains keyword
        boolean value = string.contains("Track");
        System.out.println("the boolean value is" + value);
        // To print the size of the stack
        System.out.println("the size of the queue is" + string.size());

        Stream<String> stream = string.stream();
        System.out.println("The queue elements printed using Stream ");
        stream.forEach(iteration -> System.out.print(iteration + " "));
        System.out.println();
        Queue<Integer> integer = new PriorityQueue<>();
        integer.add(10);
        integer.add(40);
        integer.add(30);
        System.out.println("The list elements are" + integer);
        System.out.println("The removerd element is" + integer.remove());
        System.out.println("the queue after removal of first element is" + integer);
        // searching using contains keyword
        boolean value1 = integer.contains(10);
        System.out.println("the boolean value is" + value1);
        // To print the size of the stack
        System.out.println("the size of the queue is" + integer.size());

        Stream<Integer> stream1 = integer.stream();
        System.out.println("The queue elements printed using Stream ");
        stream1.forEach(iteration1 -> System.out.print(iteration1 + " "));

    }


}
