/*
Requirement:
    To explain about the class loading
    
Entity:
    ClassLoadingDemo
    
Method signature:
    public static void main(String[] args)
    
Jobs to be done:
    1. Load the class java.lang.Integer and initialize the object after loading it and store it in className.
    2. Print the className and package name
    3. Create an instance of Method that stores all the default methods of Integer type.
    4. For every method in the Integer type
           4.1) Print that method name
 */
package com.java.training.reflection;

import java.lang.reflect.Method;

public class ClassLoadingDemo {
    
    public static void main(String[] args) {
        try {
            Class<?> className = Class.forName("java.lang.Integer");
            System.out.println("Class name: " + className.getName());
            System.out.println("package name: " + className.getPackage());
            Method[] methods = className.getDeclaredMethods();
            System.out.println("Methods of the class String are ");
            
            for (Method method : methods) {
                System.out.println(method.getName());
            }
        } catch (ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

}

/*
java class loaders are  used to load the class during runtime.
loading means loading the .class files.
*/