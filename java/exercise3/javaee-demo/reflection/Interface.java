/*
REQUIREMENT:

To create a Program to demonstrate Target annotation (@Target) using interface annotation (@interface)

ENTITY:
MyClass

FUNCTION DECLARATION:
public static void main(String args[])

Jobs to be done:
1. Import annotation for performing ElementType and Target.
2. Define the target type as METHOD.
4. MyCustomAnnotation is created by using @interface.
5. Apply MyCustomAnnotation to class MyClass.
6. thus Annotation are applied to methods using target annotation.

Psuedo Code:

@Target(ElementType.METHOD)
@interface MyCustomAnnotation{
  //create elements to annotation
}

public class MyClass{
  
  @MyCustomAnnotation(
  //assign value to elements

)
  public static void main(String[] args){
  System.out.println("This function is annotated");
}
}
*/


import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
@Target(ElementType.METHOD)
@interface MyCustomAnnotation{
  int studentAge() default 18;
  String studentName();
  String stuAddress();
  String stuStream() default "ECE";
}

public class Interface{
  
  @MyCustomAnnotation(
  studentName="Chaithra",
  stuAddress="Agra"

)
  public static void main(String[] args){
  System.out.println("This function is annotated");
  }
}


//Output:
//This function is annotated
