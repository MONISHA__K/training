/*
Requirement:
    To tell the difference between static loading and dynamic loading
    
Entity:
    No entity
    
Method signature:
    No method used
    
Jobs to be done:
    1. Explain the difference between static loading and dynamic loading
    
Answer:
Static Class Loading: Creating objects and instance using new keyword is known as static class 
loading. The retrieval of class definition and instantiation of the object is done at compile time.

Dynamic Class Loading: Loading classes use Class.forName () method. Dynamic class loading is done 
when the name of the class is not known at compile time.
*/
