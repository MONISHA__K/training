
/*	
REQUIREMENT:
To Deprecate the display method and to suppress the deprecate message. 

ENTITY:
DeprecatedTest
Test

FUNCTION DECLARATION:
public void display() 
public static void main(String args[])

Jobs to be done:
1. Import Annotation to use override annotation
2. create a class DeprecatedTest with method Display().
4. Declare the main class as public
5. using Deprecated annotation make the display method as deprecated method
6. hence the method fails to execute and throws an error message as " SuppressWarningTest.java uses or overrides a deprecated API."
7. To suppress the deprecated error, use SuppressWarning annotation to suppress "checked" and "deprecation" error in main class.
8. Thus the deprecation is suppressed and output is displayed.

Pseudo code:
class DeprecatedTest 
{   
    @Deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
  
public class SuppressWarningTest
{ 
    
    @SuppressWarnings({"checked", "deprecation"}) 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
*/



package com.java.training.reflection;
import java.lang.annotation.Annotation;

class DeprecatedTest { 
    @Deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
  
public class SuppressWarningTest 
{ 
    
    @SuppressWarnings({"checked", "deprecation"}) 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
