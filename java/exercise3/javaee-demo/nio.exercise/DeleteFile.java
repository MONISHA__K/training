/*
 * Requirement:
 *     Perform file operations using delete() method.
 * Entity:
 *     DeleteFile
 * Function Signature:
 *     public static void main(String[] args)
 * Jobs to be done;
 *     1) Create path instance for a file which has to be delete.
 *     2) Delete the file using delete() method.
 *     3) Print file has been deleted successfully.
 * Pseudocode:
 * 
 * class DeleteFile {

        public static void main(String[] args) throws IOException {
            //Create path instance for a file which has to be delete and name it as sourcefile.
             Files.delete(sourceFile);
            System.out.println("File has been deleted successfully");
       }
  }
 */
package com.java.training.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DeleteFile {

    public static void main(String[] args) throws IOException {

        Path sourceFile = Paths.get("E:/Filedemo/File1/source.txt");
        Files.delete(sourceFile);
        System.out.println("File has been deleted successfully");
    }

}
