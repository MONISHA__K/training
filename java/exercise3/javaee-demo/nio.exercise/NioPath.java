/*
 * Requirement:
 *     Perform file operations using move(0 method.
 * Entity:
 *     MoveFile
 * Function Signature:
 *     public static void main(String[] args)
 * Jobs to be done:
 *     1) Create a path instance for a file path which has to be move.
 *     2) Create another path instance for a file where it to be placed.
 *     3) Move the file using move() method.
 *     4) print File move successfully.
 * Pseudocode:
 *  
 *class MoveFile {
    
      public static void main(String[] args) throws IOException { 
          //Create path instance name it as sourceFile
          //Create path instance name it as moveFile.
           Files.move(sourceFile, moveFile);
           System.out.println("File moved successfully");
    } 
}

 */
package com.java.training.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MoveFile {

    public static void main(String[] args) throws IOException {

        Path sourceFile = Paths.get("E:/Filedemo/File/source.txt");

        // moving a file from one directory to another directory
        Path moveFile = Paths.get("E:/Filedemo/File1/source.txt");
        Files.move(sourceFile, moveFile);
        System.out.println("File moved successfully");
    }
}
