/*
Requirement:
    to write a program for URL clas and URLConnection class.
    
Entity:
    UrlClassDemo
    
Method signature:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create an instance of URL as urlName and give the url you want.
    2. Now print 
           2.1) protocol of the url
           2.2) host name of the url
           2.3) port of the url
     3. Now create an instance of URLConnection  that stores an object of URL class
     4. Create an String variable mimeType that stores the or returns the content type of the reference
     5. Print mimeType
     
Pseudo code:
class UrlClassDemo {
    
    public static void main(String[] args) throws Exception {
        URL urlName = new URL("http://www.google.com");
        System.out.println("protocol: " + urlName.getProtocol());
        System.out.println("port: " + urlName.getPort());
        System.out.println("Host name: " + urlName.getHost());
        
        URLConnection urlConnection = urlName.openConnection();
        String mimeType = urlConnection.getContentType();   
        System.out.println("The mime type is : "+ mimeType);
        System.out.println("The time out time of connection is : "+urlConnection.getConnectTimeout());
    }

}
*/
package com.java.training.networking;

import java.net.URL;
import java.net.URLConnection;

public class UrlClassDemo {
    
    public static void main(String[] args) throws Exception {
        URL urlName = new URL("http://www.google.com");
        System.out.println("protocol: " + urlName.getProtocol());
        System.out.println("port: " + urlName.getPort());
        System.out.println("Host name: " + urlName.getHost());
        
        URLConnection urlConnection = urlName.openConnection();
        String mimeType = urlConnection.getContentType();   
        System.out.println("The mime type is : "+ mimeType);
        System.out.println("The time out time of connection is : "+urlConnection.getConnectTimeout());
    }

}
