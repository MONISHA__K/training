/*
 * Requirement:
 *     Build a server side program using Socket class for Networking.
 * Entity:
 *     Server
 * Method Signature:
 *     public static void main(String[] args)
 * Jobs to be done:
 *     1) Open socket channel by create object and name it as server.
 *     2) Create a object for SocketAdderss and assign server name and server address.
 *     3) Connect the socket address to server.
 *     4) Print Server is ready to send.
 *     5) Close the server channel.
 * Pseudocode:
 * class Server {

      public static void main(String[] args) throws IOException {
        SocketChannel server = SocketChannel.open();
        SocketAddress socketAddress = new InetSocketAddress("localhost", 88);
        server.connect(socketAddress);

        System.out.println("Server is ready to send");
        server.close();
    }
}
 */
package com.java.training.networking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

public class Server {

    public static void main(String[] args) throws IOException {
        SocketChannel server = SocketChannel.open();
        SocketAddress socketAddress = new InetSocketAddress("localhost", 88);
        server.connect(socketAddress);

        System.out.println("Server is ready to send");
        server.close();
    }
}
