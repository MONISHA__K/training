package com.java.training.collections;

import java.util.Collections;
import java.util.Vector;

/* code for sorting vetor list in descending order.
1.Requirements:
   *vector with values
2.Entities
   * SortVectorDesending
3.Function Declaration
   -none-
4.Jobs to be done
    * Create a class name SortVectorDesending
    * put public static void main
    * Create a vector name car 
    * Add 6 values in the vector
    * sorting vetorin descending order and print
5.Pseudo Code:
public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> car = new Vector<String>();
		car.add(" ");
		System.out.println( car);
		Collections.sort(car, Collections.reverseOrder());		 
		System.out.println( car);
	}
}

 */

public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> car = new Vector<String>();
		car.add("Mazda");
		car.add("Lexus");
		car.add("Audi");
		car.add("Mecleran");
		car.add("RR");
		car.add("Dodge");
		System.out.println( car);
		Collections.sort(car, Collections.reverseOrder());		 
		System.out.println( car);
	}
}
