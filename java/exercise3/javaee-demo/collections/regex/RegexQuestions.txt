                                               
2.Demonstrate establish the difference between lookingAt() and matches()
matches()
       The matches method attempts to match the entire input sequence against the pattern.

lookingAt() 
       The lookingAt method attempts to match the input sequence, starting at the beginning, against the pattern.
---------------------*-------------------*-----------------------------*----------------------------------------
3.Demonstrate the different types of groups in regex.
         1.Capturing group
         2.Non-capturing group
         3.Backreference
         4.Relative Backreference
         5.Failed backreference
         6.Invalid backreference
         7.Nested backreference
         8.Forward reference
---------------------*-------------------*-----------------------------*----------------------------------------
4.Demonstrate all the fields of the pattern class

   The Pattern class defines an alternate compile method that accepts a set of flags affecting the way the pattern is matched. 
The flags parameter is a bit mask that may include any of the following public static fields: 
Pattern. 
    CANON_EQ Enables canonical equivalence.
---------------------*-------------------*-----------------------------*----------------------------------------

6.Difference between replaceAll() and appendReplacement().

Answer:
    The difference between replaceAll() and appendReplacement() is that the replaceAll() method replaces all the 
occurrences of old string with the new string and the appendReplacement() method of Matcher Class behaves as a append-and-replace method. 
This method reads the input string and replace it with the matched pattern in the matcher string.