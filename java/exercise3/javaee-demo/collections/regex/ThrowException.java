package com.java.training.collections.regex;
/*
 2.Create a program which throws exception and fetch all the details of the exception. 
--------------------------------------WBS---------------------------------------------
1.Requirements:
   - Create a program which throws exception and fetch all the details of the exception.
2.Entities
   - ExceptionHandling
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
  1.Create a Class name ExceptionHandling and declare main method with throws Exception.
  3.Create a string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
  4.Create a patten and put a patten
     =>^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$
     4.i) Must have at least one uppercase letter
     4.ii)Must have at least one lower case letter
     4.iii)Must have at least one digit   
  5.Create and Using Matcher and also with if condition print the result.   
 
--------------------------------Program------------------------------------------------*/

import java.io.FileNotFoundException;
import java.io.IOException;

public class ThrowException {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		try{
			testException(-5);
			testException(-10);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			System.out.println("Releasing resources");			
		}
		testException(15);
	}
	
	public static void testException(int i) throws FileNotFoundException, IOException{
		if(i < 0){
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		}else if(i > 10){
			throw new IOException("Only supported for index 0 to 10");
		}

	}

}