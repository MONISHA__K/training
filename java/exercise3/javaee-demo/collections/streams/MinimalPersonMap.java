package com.java.training.collections.streams;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API
 * 
 * Requirement
 * 1.Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API
 * 
 * Method Signature
 * 
 * Entity
 * 1.MinimalPersonMap
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store the person in persons list
 * 2.Create a List called nonEmpty 
 * 3.Check each person 
 * 		3.1)Person's name is not null
 * 		3.2)Person's email address not null
 * 4.Store the person in the nonEmpty list
 * 5.print the nonEmpty list.
 * 
 * 
 * Pseudo Code
 * 
 * class MinimalPersonMap {
 * 
 * 		public static void main(String[] args ){
 * 	
 * 		List<Person> persons = Person.createRoster();
 *		
 *		List<Person> nonEmpty = persons.stream()
 *			   .filter(person -> person.getName() != null)
 *			   .filter(person -> person.getEmailAddress() != null)
 *			   .map(person -> person)
 *			   .collect(Collectors.toList());
 *		System.out.println(nonEmpty);
 * 	}
 * }
 *
 */

public class MinimalPersonMap {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		List<Person> nonEmpty = persons.stream()
			   .filter(person -> person.getName() != null)
			   .filter(person -> person.getEmailAddress() != null)
			   .map(person -> person)
			   .collect(Collectors.toList());
		System.out.println(nonEmpty);
	}

}
