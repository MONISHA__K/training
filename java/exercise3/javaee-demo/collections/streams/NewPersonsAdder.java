package com.java.training.collections.streams;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Problem Statement
 * 1.Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
 * 		1.Print the number of persons in roster List after the above addition.
 * 		2.Remove the all the person in the roster list
 * 
 * Requirement
 * 1.Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
 * 		1.Print the number of persons in roster List after the above addition.
 * 		2.Remove the all the person in the roster list
 * 
 * Method Signature
 * 
 * Entity
 * 1.NewPersonsAdder
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Create a newRoster list and add the given persons in the list.
 * 3.Use count method to count the  number of person in the newRoster list and print it
 * 4.Use the removeAll method to remove the added person in the newRoster list.
 * 5.Check if the list has anything
 * 		5.1)if the list has any items then print it
 * 		5.2)if it does not have anything then print empty.
 * 
 * Pseudo Code
 * List<Person> newRoster = new ArrayList<>();
 *            newRoster.add(
 *               new Person(
 *               "John",
 *               IsoChronology.INSTANCE.date(1980, 6, 20),
 *               Person.Sex.MALE,
 *                "john@example.com"));
 *           newRoster.add(
 *               new Person(
 *               "Jade",
 *               IsoChronology.INSTANCE.date(1990, 7, 15),
 *               Person.Sex.FEMALE, "jade@example.com"));
 *           newRoster.add(
 *               new Person(
 *               "Donald",
 *               IsoChronology.INSTANCE.date(1991, 8, 13),
 *               Person.Sex.MALE, "donald@example.com"));
 *           newRoster.add(
 *               new Person(
 *               "Bob",
 *               IsoChronology.INSTANCE.date(2000, 9, 12),
 *               Person.Sex.MALE, "bob@example.com"));
 *               
 *           newRoster.addAll(persons);
 *           newRoster.stream().forEach(person -> System.out.println(person));
 *           
 *           System.out.println(newRoster.stream().count());
 *		
 *			 newRoster.removeAll(newRoster);
 *		
 *			 Iterator newRoIt = newRoster.iterator();
 *			 if(newRoIt.hasNext()) {
 *				System.out.println(newRoIt.next());
 *			 } else {
 *				System.out.println("Empty");
 *			 }
 *           
 *
 */

public class NewPersonsAdder {
	
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		
		List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        
        newRoster.addAll(persons);
        
        newRoster.stream().forEach(person -> System.out.println(person));
        
        //Person Count
        System.out.println(newRoster.stream().count());
        
        
        //Remove all from the newRoster list
        newRoster.removeAll(newRoster);
        
        Iterator newRoIt = newRoster.iterator();
        if(newRoIt.hasNext()) {
        	System.out.println(newRoIt.next());
        } else {
        	System.out.println("Empty");
        }
        
        
	}

}
