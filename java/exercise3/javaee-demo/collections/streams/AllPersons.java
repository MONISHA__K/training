package com.java.training.collections.streams;

import java.util.List;

/**
 * Problem Statement
 * 1.Print all the persons in the roster using java.util.Stream<T>#forEach
 * 
 * Requirement
 * 1.Print all the persons in the roster using java.util.Stream<T>#forEach
 * 
 * Method Signature
 * 
 * Entity
 * 1.AllPersons
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Check the List
 * 		2.1) If the List is Empty print List is Empty
 * 		2.2) If the List is not Empty then print the Person Details in the List
 * 
 * 
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 * persons.stream()
 * 		  .filter(person -> person != null)
 * 		  .forEach(person -> System.out.println(person));
 * 
 */

public class AllPersons {
	
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		persons.stream()
		  	   .filter(person -> person != null)
		  	   .forEach(person -> System.out.println(person));
	}

}
