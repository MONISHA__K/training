package com.java.training.collections.streams;

import java.util.Iterator;
import java.util.List;

/**
 * Problem Statement
 * 1.Iterate the roster list in Persons class and and print the person without using forLoop/Stream
 * 
 * Requirement
 * 1.Iterate the roster list in Persons class and and print the person without using forLoop/Stream
 * 
 * Method Signature
 * 
 * Entity
 * 1.AllPersonIterator
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Create a Iterator for the persons list
 * 		2.1)Check if the list has any item inside 
 * 		2.2)If there is a next item then print the item.	
 * 
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 * Iterator iterator = persons.iterator();
 *		
 * if(iterator.hasNext()) {
 *		System.out.println(iterator.next());
 * }
 * 
 *
 */

public class AllPersonIterator {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		Iterator iterator = persons.iterator();
		if(iterator.hasNext()) {
			 System.out.println(iterator.next());
		}
	}
}
