package com.java.training.collections.streams;

import java.util.List;

/**
 * Problem Statement
 * 1.Write a program to find the average age of all the Person in the person List
 * 
 * Requirement
 * 1.Write a program to find the average age of all the Person in the person List
 * 
 * Method Signature
 * 
 * 
 * Entity
 * 1.AveragePerson
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.For each Person
 * 		2.1)Add each person's age as int by invoking the getAge method from Person Class
 * 		2.2)Calculate the average and save the average age in allAge.
 * 3.Print the allAge.
 * 
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 *		
 * double allAge = persons.stream()
 *					      .mapToDouble(Person::getAge)
 *						  .average()
 *						  .getAsDouble();
 *		
 * System.out.println(allAge);
 * 
 *
 */

public class AveragePerson {
	
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		
		double allAge = persons.stream()
							   .mapToDouble(Person::getAge)
							   .average()
							   .getAsDouble();
		
		System.out.println(allAge);
	}

}
