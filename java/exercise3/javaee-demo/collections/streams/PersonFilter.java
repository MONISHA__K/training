package com.java.training.collections.streams;

import java.util.List;
import java.util.Optional;

import com.java.training.collections.streams.Person.Sex;

/**
 * Problem Statement
 * 1.Write a program to filter the Person, who are male and
 *       - find the first person from the filtered persons
 *       - find the last person from the filtered persons
 *       
 * Requirement
 * 1.Write a program to filter the Person, who are male and
 *       - find the first person from the filtered persons
 *       - find the last person from the filtered persons
 *       
 * Method Signature
 * 
 * 
 * Entity
 * 1.PersonFilter
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store it in filterPerson list
 * 2.For each Person
 * 		2.1)Check if the gender is equal to MALE and first in list.
 * 		2.2)store the person in the firstPerson of PersonFilter type.
 * 		2.3)Check if the gender is equal to MALE and last in list
 * 		2.4)Store the person in the lastPerson of PersonFilter type.
 * 3.Get any person form the list and store in randomPerson of PersonFilter type.
 * 4.Print the firstPerson, lastPerson, randomPerson.
 * 
 * Pseudo Code
 * 
 * class PersonFilter {
 * 	
 * 		public static void main(String[] args) {
 * 			
 * 			List<PersonFilter> filterPerson = Person.createRoster();
 *
 *			//FirstPerson
 *			Optional<PersonFilter> firstPreson = filterPerson.stream()
 *															 .filter(person -> person.getGender() == Sex.MALE)
 *															 .findFirst();
 *			//LastPerson
 * 			Person lastPerson = filterPerson.stream()
 * 											 				.filter(person -> person.getGender() == SEX.MALE)
 * 										     				.reduce((first,last) -> last)
 * 											 				.orElse(null);
 * 			//RandomPerson
 * 			Optional<PersonFilter> anyPerson = filterPerson.stream()
 *														   .filter(person -> person.getGender() == Sex.MALE)
 *														   .findAny();
 *														   
 * 			System.out.println(firstPerson);
 * 			System.out.println(lastPerson);
 * 			System.out.println(anyPerson);
 * 		}
 * }
 *
 */

public class PersonFilter {
	
	public static void main(String[] args) {
		List<Person> filterPerson = Person.createRoster();
		 
		//FirstPerson
		Optional<Person> firstPerson = filterPerson.stream()
												   .filter(person -> person.getGender() == Sex.MALE)
												   .findFirst();
		//LastPerson
		Person lastPerson = filterPerson.stream()
												  .filter(person -> person.getGender() == Sex.MALE)
												  .reduce((first,last) -> last)
												  .orElse(null);
		//RandomPerson
		Optional<Person> anyPerson = filterPerson.stream()
												 .filter(person -> person.getGender() == Sex.MALE)
												 .findAny();
		System.out.println(firstPerson.toString());
		System.out.println(lastPerson.toString());
		System.out.println(anyPerson.toString());
	}

}
