package com.java.training.collections.streams;

import java.time.LocalDate;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.Write a program to filter the Person, who are male and age greater than 21
 * 
 * 
 * Requirement
 * 1.Write a program to filter the Person, who are male and age greater than 21
 * 
 * 
 * Method Signature
 * 
 * Entity
 * 1.Person
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.For each person 
 * 		2.1)Check If person's Gender is MALE and person's AGE is greater than 21.
 *  	2.2)Add the filtered person to filteredPersons.
 * 3.Print the filteredPersons.
 * 
 * 
 * Pseudo Code
 * class Person {
 * 		public static void main(String[] args) {
 *         	//1st Question
 * 			List<Person> persons = Person.createRoster();
 * 			List<Person> filteredPersons = persons.stream()
 *       								   	      .filter(person -> person.gender == person.Sex.MALE)
 *       								   		  .filter(person -> person.age > 21)
 *       								   		  .collect(Collectors.toList());
 *       
 *       	System.out.println(filteredPersons.toString());
 *
 * 		}
 * }
 * 
 */
public class Person {
	String name; 
    LocalDate birthday;
    Sex gender;
    String emailAddress;
    
    public enum Sex {
        MALE, FEMALE
    }
    
    public Person() {
    	
    }
	public Person(String nameArg, LocalDate birthdayArg,
	        Sex genderArg, String emailArg) {
	        name = nameArg;
	        birthday = birthdayArg;
	        gender = genderArg;
	        emailAddress = emailArg;
	}

    public int getAge() {
        return birthday
            .until(IsoChronology.INSTANCE.dateNow())
            .getYears();
    }

    public void printPerson() {
      System.out.println(name + ", " + this.getAge());
    }
    
    public Sex getGender() {
        return gender;
    }
    
    public String getName() {
        return name;
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public LocalDate getBirthday() {
        return birthday;
    }
    
    public static int compareByAge(Person a, Person b){
        return a.birthday.compareTo(b.birthday);
    }

    public static List<Person> createRoster() {
    	List<Person> roster = new ArrayList<>();
        
        roster.add(
            new Person(
            "Fred",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,"fred@example.com"));
        roster.add(
            new Person(
            "Jane",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jane@example.com"));
        roster.add(
            new Person(
            "George",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "george@example.com"));
        roster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        
        return roster;
    }
	
	public static void main(String[] args) {
		
		//Person with Gender MALE and AGE > 21 
		List<Person> persons = Person.createRoster();
		List<Person> filteredPersons = persons.stream()
				 					   	      .filter(person -> person.gender == Sex.MALE)
				 					   	      .filter(person -> person.getAge() > 21)
				 					   	      .collect(Collectors.toList());
		System.out.println(filteredPersons.toString());
	}
}
