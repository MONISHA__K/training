package com.java.training.collections;

import java.util.Set;
import java.util.stream.Stream;
import java.util.HashSet;
import java.util.Scanner;

/*
 * 1.Requirement
 *   Java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 * 2.Entity
 *   HashSet
 *
 * 3.Function Declaration
 *   void add(Set<S> set, int val);
 *   void remove(Set<S> set, int val);
 *   void fullSet(Set<S> set);
 *
 * 4.Jobs to be Done
 *   1.Create a HashSet of some type.
 *   2.Get the Value from the User.
 *   3.Call void add method to add elements to Set.
 *   4.Call void remove to remove elements to Set. 
 *   5.Call void fullSet method to display the Elements
 *   6.Use stream API to iterate the Set.
 * 
 * 
 * 5.Pseudo Code
 * class HashSetExercise {
 *	
 *	public HashSetExercise() {
 *		
 *	}
 *	
 *	void remove(Set<Integer> set, Integer val) {
 *		set.remove(val);
 *	}
 *	
 *	void fullSet(Set<Integer> set) {
 *		set.stream().forEach(x -> System.out.println(x));
 *	}
 *	
 *	public static void main(String[] args) {
 *		HashSetExercise hash = new HashSetExercise();
 *		
 *		Scanner scanner = new Scanner(System.in);
 *		
 *		Set<Integer> newSet = new HashSet<Integer>();
 *		
 *		newSet.add(10);
 *		newSet.add(20);
 *		newSet.add(30);
 *		newSet.add(40);
 *		newSet.add(50);
 *		newSet.add(60);
 *		
 *		hash.fullSet(newSet);
 *		
 *		System.out.println("Enter the Value to Remove");
 *		
 *		Integer val = scanner.nextInt();
 *		
 *		hash.remove(newSet, val);
 *		
 *		newSet.stream().forEach(x -> System.out.println(x));
 *		
 *		
 *	} 
 *
 *}
 *
 */

public class HashSetExercise {
	
	public HashSetExercise() {
		
	}
	
	void remove(Set<Integer> set, Integer val) {
		set.remove(val);
	}
	
	void fullSet(Set<Integer> set) {
		set.stream().forEach(x -> System.out.println(x));
	}
	
	public static void main(String[] args) {
		HashSetExercise hash = new HashSetExercise();
		
		Scanner scanner = new Scanner(System.in);
		
		Set<Integer> newSet = new HashSet<Integer>();
		
		newSet.add(10);
		newSet.add(20);
		newSet.add(30);
		newSet.add(40);
		newSet.add(50);
		newSet.add(60);
		
		hash.fullSet(newSet);
		
		System.out.println("Enter the Value to Remove");
		
		Integer val = scanner.nextInt();
		
		hash.remove(newSet, val);
		
		newSet.stream().forEach(x -> System.out.println(x));
		
		
	}

}
