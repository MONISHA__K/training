package com.java.training.collections;

/*Addition,Substraction,Multiplication and Division concepts are 
achieved using Lambda expression and functional interface.
1.Requirements:
   * 2 Integers
2.Entities
   * LambdaExpDemo
   * Arthmetic - Interface 
3.Function Declaration
   * int operation(int a, int b)
4.Jobs to be done
    * Create a class name LambdaExpDemo
    * Create a interface name Arithmetic
    * inside that put method int operation(int a, int b)
    * put public static void main
    * Inside that Addition,Substraction,Multiplication and Division concepts are 
achieved using Lambda expression 
    * Print these Values
5.Pseudo Code:
interface Arithmetic {
	int operation(int a, int b);
}
  */

interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExpDemo {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(5, 6));
		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(5, 3));
		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(4, 6));
		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12, 6));
	}
}
