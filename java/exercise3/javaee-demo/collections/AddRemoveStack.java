package com.java.training.collections;

import java.util.Stack;
/*add and remove the elements in stack
1.Requirements:
   * stack with values
2.Entities
   * AddRemoveStack
3.Function Declaration
   -none-
4.Jobs to be done
    * Create a class name AddRemoveStack
    * put public static void main
    * Create a stack name fruits
    * Add 4 values in the list
    * pop top element
    * print the stack
5.Pseudo Code:
public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //stack using generic type
        fruits.push
 		fruits.pop() 
 */

public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //stack using generic type
        fruits.push("Apple"); //adding a element to stack
        fruits.push("Banana");
        fruits.push("Mango");
        fruits.push("Orange"); 
        fruits.pop();  //removing a element to stack
        System.out.println(fruits);
    }
}
