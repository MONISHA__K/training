package com.java.training.collections;

import java.util.ArrayList;
import java.util.List;

/*8 districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy  
     to be converted to UPPERCASE.
1.Requirements:
   * List with values
2.Entities
   * ConvertIntoUpperCase
3.Function Declaration
   -none-
4.Jobs to be done
    * Create a class name ConvertIntoUpperCase
    * put public static void main
    * Create a list name city
    * Add 8 values in the list
    * converted to UPPERCASE
    * Print the list 
5.Pseudo Code:
public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add(" "); 
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }    
     */


public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add("Madurai"); 	//Add 8 values in the list
	city.add("Coimbatore");
	city.add("Theni");
	city.add("Chennai");
	city.add("Karur");
	city.add("Salem");
	city.add("Erode");
	city.add("Trichy ");
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }
    }
}
