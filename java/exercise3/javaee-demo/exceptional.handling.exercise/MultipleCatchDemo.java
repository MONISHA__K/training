package com.java.training.core.Exception;

/*
Requirement:
    To Demonstrate catching multiple exception.

Entity:
    public class MultipleCatchDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Declare the class MultipleCatchDemo as public.
    3. Under a main method declare a try block.
    4. The try block contains the initialization of an integer array named array of size 5.
    5. Now declare the value of the index as 30 / 0.
    6. Then print the array.
    7. Now give multiple exception block stating Arithmetic exception ArrayIndexOutOfBoundsException.
    8. Finally give the print statement "rest of code"

Pseudo code:
class MultipleCatchDemo {

    public static void main(String[] args) {
        try {
            int[] array = new int[5];
            array[5] = 30 / 5;
            System.out.println(array[5]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(message1);
        } catch (ArithmeticException e) {
            System.out.println(message2);
        } catch (Exception e) {
            System.out.println(message3);
        }

    }
}  
*/

public class MultipleCatchDemo {
    
    public static void main(String[] args) {
        try {    
            int[] array=new int[5];    
            array[5]=30/0;    
            System.out.println(array[10]);  
           } catch (ArithmeticException e) { 
               System.out.println("Arithmetic Exception occurs");  
           } catch(ArrayIndexOutOfBoundsException e) { 
               System.out.println("ArrayIndexOutOfBounds Exception occurs");  
           } catch(Exception e) {
               System.out.println("Parent Exception occurs");  
           }             
           
        System.out.println("rest of the code");    
    }
}
