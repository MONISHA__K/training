package com.java.training.core.Exception;

/*
Requirement:
    To demonstrate the difference between throw and throws.

Entity:
    public class ThrowThrowsDiffernceDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Declare the class ThrowThrowsDiffernceDemo as public.
    3. create a method chackAge() to demonstrate the example for throw.
    4. Create a method division() to demonstrate throws clause.
    5. print the results. 

Pseudo code:
class ThrowThrowsDifferenceDemo {

    public void checkAge(int age) {

        // Throw keyword is used to throw an exception explicitly.
        if (age < 18) {
            throw new ArithmeticException(message);
        } else {
            System.out.println(message);
        }
    }

    // Throws is used to declare an exception, which means it works similar to the try-catch.
    public void division(int number1, int number2) throws ArithmeticException {
        int quotient = number1 / number2;
        System.out.println(quotient);
    }

    public static void main(String args[]) {
        ThrowThrowsDifferenceDemo object = new ThrowThrowsDifferenceDemo();
        object.checkAge(age);

        ThrowThrowsDifferenceDemo object1 = new ThrowThrowsDifferenceDemo();
        try {
            object1.division(number1, number2);
        } catch (ArithmeticException exception) {
            System.out.println(message);
        }
    }
}
    
*/

public class ThrowThrowsDiffernceDemo {

    public void checkAge(int age) {
        
        // Throw keyword is used to throw an exception explicitly.
        if (age < 18) {
            throw new ArithmeticException("Not Eligible for voting");
        } else {
            System.out.println("Eligible for voting");
        }
    }

    // Throws is used to declare an exception, which means it works similar to the try-catch.
    public int division(int a, int b) throws ArithmeticException {
        int t = a / b;
        return t;
    }

    public static void main(String args[]) {
        ThrowThrowsDiffernceDemo object = new ThrowThrowsDiffernceDemo();
        object.checkAge(13);
        System.out.println("End Of Program");
        
        ThrowThrowsDiffernceDemo object1 = new ThrowThrowsDiffernceDemo();
        try {
           System.out.println(object1.division(15,0));  
        }
        catch (ArithmeticException e) {
           System.out.println("You shouldn't divide number by zero");
        }
    }
}


