/*Requirement:
 * 		 To write primitive datatypes to file using by dataoutputstream.
 * 
 * Entity:
 * 		FilterAndBuffer
 * 
 * Method Signature:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1) Open a file to write the primitive data type values in FileOutputStream.
 * 		2) Declare that particular file as DataOutputStream type.
 * 		3) Write some values which are primitive type.
 * 
 * Pseudo Code:
 * 		class FilterAndBuffer {
 * 			public static void main(String[] args) {
 * 				//Create a FileOutputStream
 * 				FileOutputStream file = new FileOutputStream("File.name");
 * 
 * 				DataOutputStream dos = new DataOutputStream(file);
 * 				//Add some primitive data values to the file.
 * 
 * 				//To print the file, Access the file again in datainputstream.
 * 			}
 * 		}
 */
package com.java.training.io;

import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class DataOutputStreamDemo {
    public static void main(String[] args) throws IOException {

        FileOutputStream fos = new FileOutputStream("data.da");
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeInt(50000);
        dos.writeShort(2500);
        dos.writeByte(120);
        dos.close();

        FileInputStream fis = new FileInputStream("data.da");
        DataInputStream dis = new DataInputStream(fis);
        System.out.println("Int   : " + dis.readInt());
        System.out.println("Short : " + dis.readShort());
        System.out.println("Byte  : " + dis.readByte());
        dis.close();
    }

}
