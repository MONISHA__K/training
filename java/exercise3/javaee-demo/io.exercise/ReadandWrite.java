/*
 * Requirement :
 *  write a Java program reads data from a particular file using FileReader and writes it to another, using FileWriter .
 *  
 *Entity:
 *  ReadandWrite 
 *  
 *Function Signature:
 *  public static void main(String[] args)
 *  
 *Jobs to be Done:
 *  1)Create an object for FileInputStream and declare the path of the file .
 *  2)Create an object for FileWriter and declare the path of the filev.
 *  2)Read the file and store the value in a integer variable.
 *  3)Until the ascii value comes -1 read the file.
 *      3.1)Convert the ascii value to character and store it on a String variable .
 *      3.2)write the find using the String variable.  
 *  4)Print the statement .
 *  5)Close the file reader and writer .
 *  
 *Pseudo code:
 *
 *class ReadandWrite {
    public static void main(String[] args) throws IOException {
        //Declare the file path in FileInputStream 
        FileInputStream reader = new FileInputStream("d:\\laptop.txt");
        FileWriter writer= new FileWriter("d:\\output.txt");
        int data = reader.read();
        while (data != -1) {
            char dataChar = (char) data;
            writer.write(dataChar);
            data = reader.read();
        }
        System.out.println("Read and write operation was done successfully ");
        reader.close();
            
        writer.close();
    }
}
 */

package com.java.training.io;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class ReadandWrite {
    public static void main(String[] args) throws IOException {
        FileInputStream reader = new FileInputStream("laptop.txt");
        FileWriter writer= new FileWriter("output.txt");
        int data = reader.read();
        while (data != -1) {
            char dataChar = (char) data;
            writer.write(dataChar);
            data = reader.read();
        }
        System.out.println("Read and write operation was done successfully ");
        reader.close();
            
        writer.close();
    }
}
