package com.java.training.core.list;

/*
Requirement:
    To describe about the contains() method and isEmpty() method.

Entity:
    public class SetExampleDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a package com.java.training.core.list.
    2. Import a package Set and HashSet.
    3. Declare a class SetExampleDemo as public.
    4. Create a object setA for the class Set and add the elements to it.
    5. Use Contains and isEmpty() methods and print the result.
Pseudo code:
class SetMethod {

    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        // add elements
        setA.add(elements);
        System.out.println("Does the Set setA contains Writer" + " " + setA.contains(element));
        System.out.println("IS the Set is empty" + " " + setA.isEmpty());
    }
}
*/

import java.util.Set;
import java.util.HashSet;

public class SetMethodDemo {
    
    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        setA.add("Hero");
        setA.add("Heroien");
        setA.add("Director");
        setA.add("Editor");
        setA.add("Music Director");
        System.out.println("Does the Set setA contains Writer" + " " + setA.contains("Writer"));
        System.out.println("IS the Set is empty" + " " + setA.isEmpty());
        
    }

}
