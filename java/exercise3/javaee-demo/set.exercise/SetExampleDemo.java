package com.java.training.core.list;

/*
Requirement:
    To create a table and to add elements and to use addAll() and removeAll().
    
Entity:
    public class SetExampleDemo
    
Function Declaration:
    public static void main(String[] args)
    
jobs to be done:
    1. Create a package com.java.training.core.list.
    2. Import the Set and HashSet package.
    3. Create a class SetExampleDemo.
    4. Create an object for the Sets as string and string1 and add the elements to it.
    5. Now add all the contents of set1 to set2 and remove it using removeAll().
    6. Use iterator and forEach loop and print it. 
 Pseudo code:
class SetDemo {

    public static void main(String[] args) {
        Set<String> string = new HashSet<>();
        // add elements
        string.add(elements);

        Set<String> string2 = new HashSet<>();
        string2.add(elements);
        System.out.println("the second set is" + " " + string2);

        string2.addAll(string);
        System.out.println("the modified set is " + " " + string2);

        // Using iterator
        Iterator<String> iterator = string.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.print(element + " ");
        }

        // using for each loop
        for (String value : string) {
            System.out.print(value + " ");
        }
        System.out.println("the removed set is" + " " + string.removeAll(string2));
    }
}
*/

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetExampleDemo {
    
    public static void main(String[] args) {
        Set<String> string = new HashSet<>();
        string.add("Cricket");
        string.add("Volleyball");
        string.add("Football");
        string.add("Kabadi");
        string.add("Hockey");
        string.add("Ice Skating");
        string.add("BasketBall");
        string.add("Throwball");
        string.add("Rugby");
        string.add("Tennis");
        System.out.println("The Set1 is " + " " + string);
        
        Set<String> string2 = new HashSet<>();
        string2.add("Dhoni");
        string2.add("Prabakaran");
        System.out.println("the second set is" + " " + string2);
        
        string2.addAll(string);
        System.out.println("the modified set is " + " " + string2);
        
        // to remove all the elements
        System.out.println(" the removed set is" + " " + string.removeAll(string2));
        
        // Using iterator
        Iterator<String> iterator = string.iterator();
        
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println("using iterator " + " " + element);
        }
        
        // using for each loop
        for (String value : string) {
            System.out.println("for each loop" + " " + value);
        }
    }
}
