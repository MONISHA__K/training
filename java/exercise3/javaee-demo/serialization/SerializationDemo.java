/*
 * What is Serialization?
 * 
 * 		Serialization is a mechanism of converting the state of an object into a byte stream.
 * Serialization in Java is the process of converting the Java code Object into a Byte Stream, to
 * transfer the Object Code from one Java Virtual machine to another and recreate it using the
 * process of Deserialization.
 * 
 */
/*
 * Requirement: 
 * 		Create a File, get the Following fields as input from the user 
 * 			1.Name 
 * 			2.studentId
 * 			3.Address 
 * 			4.Phone Number 
 * 		now store the input in the File and serialize it, and again de serialize the File and print the content.
 *  
 * Entity: 
 *  	Student implements java.io.serializable
 *  	SerializationDemo
 *  
 * Function Signature: 
 * 		public static void main(String[] args) 
 * 
 * Jobs to be done: 
 * 		1)Get name, student id, address, phone number as input from the user and store it in fields which implements serializable class.
 * 		2)Try to store that values to a file.
 * 			2.1)if fails print the error details.
 * 		3)Print the details are stored successfully.
 * 		4)Try to read all the details.
 * 			4.1)if error occurs print error details.
 * 		5)Print the name, student id, address, phone number.
 * 
 * Pseudocode:
 * 
 *  class Student implements java.io.Serializable {
 *		public String name;
 *		public int student_id;
 *		public String address;
 *		public long phone_number;
 *	
 *	}
 *	
 *	
 *	public class SerializationDemo {
 *	
 *		public static void main(String[] args) {
 *			Student student = new Student();
 *	 		Scanner scanner = new Scanner(System.in);
 *			student.name = scanner.next();
 * 	
 *			student.student_id = scanner.nextInt();
 *	
 *			student.address = scanner.nextLine();
 *	
 *			student.phone_number = scanner.nextLong();
 *			scanner.close();
 *	
 *			try {
 *				FileOutputStream file = new FileOutputStream("serializeDemo.ser");
 *				ObjectOutputStream out = new ObjectOutputStream(file);
 *				out.writeObject(student);
 *				out.close();
 *				file.close();
 *				System.out.println("Serialization has been done successfully");
 *			} catch (IOException exception) {
 *				exception.getStackTrace();
 *			}
 *	
 *			Student student1 = null;
 *	
 *			try {
 *				FileInputStream fileIn = new FileInputStream("serializeDemo.ser");
 *				ObjectInputStream in = new ObjectInputStream(fileIn);
 *				student1 = (Student) in.readObject();
 *				in.close();
 *				fileIn.close();
 *			} catch (IOException inputException) {
 *				inputException.printStackTrace();
 *				return;
 *			} catch (ClassNotFoundException classnotfound) {
 *				classnotfound.printStackTrace();
 *			}
 *	
 *			System.out.println("After DeSerialization : ");
 *			System.out.println("Name: " + student1.name);
 *			System.out.println("Student_id: " + student1.student_id);
 *			System.out.println("Address: " + student1.address);
 *			System.out.println("Number: " + student1.phone_number);
 *	
 *		}
 *	
 *	}
 */
package com.java.training.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

class Student implements java.io.Serializable {
	public String name;
	public int student_id;
	public String address;
	public long phone_number;

}


public class SerializationDemo {

	public static void main(String[] args) {
		Student student = new Student();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter student name: ");
		student.name = scanner.nextLine();

		System.out.println("Enter student id: ");
		student.student_id = scanner.nextInt();

		System.out.println("Enter student address: ");
		scanner.nextLine();
		student.address = scanner.nextLine();

		System.out.println("Enter student phone number: ");
		student.phone_number = scanner.nextLong();

		scanner.close();

		try {
			FileOutputStream file = new FileOutputStream("serializeDemo.ser");
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(student);
			out.close();
			file.close();
			System.out.println("Serialization has been done successfully");
		} catch (IOException exception) {
			exception.getStackTrace();
		}

		Student student1 = null;

		try {
			FileInputStream fileIn = new FileInputStream("serializeDemo.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			student1 = (Student) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException inputException) {
			inputException.printStackTrace();
			return;
		} catch (ClassNotFoundException classnotfound) {
			classnotfound.printStackTrace();
		}

		System.out.println("After DeSerialization : ");
		System.out.println("Name: " + student1.name);
		System.out.println("Student_id: " + student1.student_id);
		System.out.println("Address: " + student1.address);
		System.out.println("Number: " + student1.phone_number);

	}

}
