SELECT first_name
  FROM employee
 WHERE dep_id IN (SELECT dep_id 
  FROM department
 WHERE dep_id=100);

SELECT first_name
  FROM employee
 WHERE dep_id = ANY (SELECT dep_id FROM department WHERE dep_id=101);

SELECT first_name,sur_name
  FROM employee
 WHERE annual_salary>=200000 OR dep_id=100;


SELECT first_name,sur_name
  FROM employee
 WHERE NOT dep_id=100;

SELECT first_name,sur_name
  FROM employee
 WHERE annual_salary>=200000 AND dep_id=100;

SELECT first_name
  FROM employee
 WHERE first_name NOT LIKE 'a%';

SELECT first_name
  FROM employee
 WHERE first_name LIKE 'a%';

SELECT first_name
  FROM employee
 WHERE dep_id = ALL (SELECT dep_id FROM department WHERE dep_id=101);



SELECT *
  FROM first_name.employee
 ORDER BY emp_id ASC LIMIT 1;

SELECT *
  FROM first_name.employee
 ORDER BY emp_id DESC LIMIT 1;


