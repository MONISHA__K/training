SELECT employee.first_name,employee.sur_name
  FROM employee 
 WHERE dep_id IN
       ( SELECT dep_id 
           FROM department 
          WHERE dep_id=100 );

SELECT * 
  FROM new_schema.employee
       CROSS JOIN  new_schema.department;


SELECT first_name,dep_name
  FROM employee
       INNER JOIN department
       ON employee.dep_id = department.dep_id;

SELECT first_name,dep_name
  FROM employee
       RIGHT JOIN department
       ON employee.dep_id = department.dep_id;


SELECT first_name,dep_name
  FROM employee
       LEFT JOIN department
       ON employee.dep_id = department.dep_id;


