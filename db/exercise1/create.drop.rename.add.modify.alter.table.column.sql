
CREATE TABLE employee1 (
             emp_id                  INT
             ,surname                VARCHAR(255)
             ,first_name             VARCHAR(255)
             ,dob                    DATE
             ,dep_id                 INT
             ,annual_salary          INT
             ,PRIMARY KEY (emp_id)
             ,CONSTRAINT 
                  UNIQUE (emp_id)
             ,CONSTRAINT 
                   CHECK (dob>'1990-06-01')
);
    
CREATE INDEX index_salary
    ON employee ( annual_salary );   

 ALTER TABLE new_schema.employee1
   ADD FOREIGN KEY (dep_id) REFERENCES new_schema.department(dep_id);


 ALTER TABLE employee1 RENAME COLUMN surname TO sur_name;

 ALTER TABLE employee1
   ADD address char;

  ALTER TABLE employee1
 MODIFY COLUMN address varchar(45);

  ALTER TABLE new_schema.employee1 DROP COLUMN address;

 INSERT INTO employee1 (
             emp_id
            ,sur_name
            ,first_name
            ,dob
            ,dep_id)
 VALUES
             '1'
            ,'raj'
            ,'Prithvi'
            ,'2000-06-09'
            ,'100'
);

TRUNCATE TABLE employee1;



