
CREATE VIEW `[All]` AS
SELECT dep.dep_name , emp1.dep_id ,emp.dob, emp.firstname
  FROM employee emp,employee1 emp1,department dep
  JOIN new_schema.employee 
    ON employee.dep_id = department.dep_id
  JOIN new_schema.employee 
    ON employee1.dep_id = department.dep_id
;

CREATE VIEW `[New_employee]` AS
SELECT department.dep_name,employee.dep_id,employee.first_name
  FROM new_schema.employee
 WHERE employee.dep_id=null
;
SELECT * 
  FROM `[New_employee]`
;

  DROP VIEW `[New_employee]`