SELECT first_name
  FROM employee
 WHERE dep_id=100;

UPDATE employee
   SET annual_salary = 200000
 WHERE dep_id=100;

DELETE FROM employee
 WHERE dep_id=NULL;

#without where

UPDATE new_schema.employee
   SET first_name = 'Prasanth';

DELETE FROM new_schema.employee;

