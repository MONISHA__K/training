
SELECT department.dep_name, employee.first_name, employee1.emp_id
  FROM new_schema.department
  JOIN new_schema.employee 
    ON employee.dep_id = department.dep_id
  JOIN new_schema.employee1 
    ON employee1.dep_id=department.dep_id ;

CREATE TABLE  new_schema.employee1(
              emp_id INT NOT NULL
             ,sur_name VARCHAR(45) NOT NULL
             ,first_name VARCHAR(45) NOT NULL
             ,dob VARCHAR(45) NOT NULL
             ,dep_id INT NOT NULL
             ,PRIMARY KEY (emp_id)
);
INSERT INTO employee1(
		    emp_id
            ,surname
            ,first_name
            ,dob
            ,dep_id
            ) 
            
     VALUES ('6'
            ,'Kailash'
            ,'R'
            ,'2000-09-08'
            ,'100')

             ,('5'
             ,'Marutha'
             ,'R'
             ,'1997-09-07'
             ,'101')
             ,('2'
             ,'Suresh'
             ,'M'
             ,'1999-04-02'
             ,'104')

             ,('3'
             ,'Kumarvraman'
	         ,'L'
             ,'1999-07-08'
             ,'103')

             ,('4'
             ,'Kala'
             ,'H'
             ,'1997-07-24'
             ,'104');

SELECT  'employee',emp_id,sur_name,first_name,dob,dep_id
  FROM  new_schema.employee
 UNION
SELECT 'employee1', emp_id,sur_name,first_name,dob,dep_id
  FROM  new_schema.employee1;

SELECT first_name
  FROM  new_schema.employee
 UNION ALL
SELECT first_name 
  FROM  new_schema.employee1
 ORDER BY first_name

