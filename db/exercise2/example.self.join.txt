when we want to display two tables under certain condition and without using the join keyword is know as self join.
In which the table has a foreign key which references its own primary key.which refers the same column in both tables

Example 
SELECT e.first_name
      ,d.name AS Department
  FROM employee e
      ,department d 
WHERE e.department_id=d.id

In this example
 we displayed the first_name from the employee table and department name from the department table and
employee table is referred as e and department table as d for easy convenience and this is known as aliasing.
By using the where condition we checked that the department_id in employee table is equal to the id in the department table.
In this department_id is the foreign key in employee table and primary key in the department table.

employee table                                      department table
+------------+------------+                         +---------+----------+ 
| first_name |  dept_id   |                         |  id     |   name   |
+------------+------------+                         +---------+----------+
|  NIKIL  |     101    |                         |  101    | Facility |
| KARTHIK  |    102    |                         |  102    | HR       |
| SHIVA  |     103   |                         |  103    | ITDesk   |
+------------+------------+                         +---------+----------+

With WHERE Condition

+------------+-----------------+
| first_name |    Department   |
+------------+-----------------+
|  sabari    |     HR          |
|  ramkuamr  |     ITDesk      |
|  santheep  |     Facility    |
+------------+-----------------+

Without the WHERE Condition


+------------+-----------------+
| first_name |    Department   |
+------------+-----------------+
|  sabari    |     HR          |
|  sabari    |     HR          |
|  sabari    |     HR          |
|  ramkuamr  |     ITDesk      |
|  ramkuamr  |     ITDesk      |
|  ramkuamr  |     ITDesk      |
|  santheep  |     Facility    |
|  santheep  |     Facility    |
|  santheep  |     Facility    |
+------------+-----------------+