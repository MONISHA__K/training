CREATE TABLE  employee(
              emp_id          INT
             ,first_name      VARCHAR(45) NOT NULL
             ,sur_name        VARCHAR(45) 
             ,dob             DATE        NOT NULL 
             ,date_of_joining DATE        NOT NULL
             ,annual_salary   INT         NOT NULL
             ,PRIMARY KEY( emp_id )
  );
  
 CREATE TABLE  department(
               dept_id   INT         NOT NULL 
              ,dept_name VARCHAR(45) NOT NULL
              ,PRIMARY KEY (dept_id)
 );
 
