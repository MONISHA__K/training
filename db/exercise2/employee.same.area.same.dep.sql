alter table employee
add column area varchar (45);

SELECT employee.first_name
      ,department.dep_name AS department
	  ,employee.area
  FROM employee employee
      ,department department 
 WHERE employee.dep_id=department.dep_id 
  AND employee.area IN('coimbatore');
  
SELECT employee.first_name
      ,department.dep_name AS department
      ,employee.area 
  FROM employee employee
      ,department department 
WHERE employee.dep_id=department.dep_id 
  AND department.dep_name IN('Recruitment') 
ORDER BY department.dep_name;

SELECT employee.first_name
      ,department.dep_name AS department
      ,employee.area 
  FROM employee employee
     ,department department 
WHERE employee.dep_id=department.dep_id 
  AND employee.area IN('coimbatore','tirpur') 
  AND department.dep_name IN('Recruitment','HR') 
ORDER BY department.dep_name;