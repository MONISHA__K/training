   USE `database`;

SELECT  employee.first_name
       ,department.dept_name AS department
       ,employee.area 
  FROM  employee employee
       ,department department 
 WHERE employee.dept_id=department.dept_id 
   AND employee.area IN('coimbatore') 
   AND department.dept_name IN('ITdesk') 
 ORDER BY department.dept_name;