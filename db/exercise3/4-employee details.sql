SELECT name as employee_name
      ,dob
      ,mail
      ,phone
      ,col_name
      ,city
      ,dept_code
      ,dept_name
      ,desg_name
      ,desgn_rank
  FROM university.employee employee
 
  LEFT JOIN university.college college
    ON employee.college_id = college.col_id
  LEFT JOIN university.college_department
    ON employee.cdept_id = college_department.cdept_id
  LEFT JOIN university.department department
    ON college_department.udept_code = department.dept_code
  LEFT JOIN university.designation designation
    ON employee.desg_id = designation.desg_id
    WHERE college.univ_code = 'A001'
 ORDER BY desgn_rank
 and col_name;