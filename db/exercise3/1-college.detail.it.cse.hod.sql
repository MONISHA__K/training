SELECT col_code
      ,col_name
      ,city
      ,state
      ,year_opened
      ,dept_name
      ,name as employee_name
  FROM university.college college
  LEFT JOIN university.university university
    ON college.univ_code = university.university_code
 RIGHT JOIN university.department department
    ON university.university_code = department.univ_code
  LEFT JOIN university.employee employee 
    ON college.col_id = employee.college_id
 WHERE dept_name IN ('CSE','IT')
   AND desg_id = '1'
 GROUP BY col_code
 