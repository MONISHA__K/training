use `university`;

select u.university_name
  ,s.id as roll_number
  ,s.stu_name as student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.col_name
  ,d.dept_name
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
 where c.univ_code = u.university_code 
 and u.university_code = d.univ_code 
 and cd.college_id = c.col_id 
 and cd.udept_code = d.dept_code
 and s.coll_id = c.col_id
 and s.cdep_id = cd.cdept_id
 and s.academic_year = '2019'
 and u.university_code = '111'
 and s.address ='coimbatore';