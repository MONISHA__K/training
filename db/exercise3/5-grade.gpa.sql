SELECT roll_no
      ,stu_name
      ,gender
      ,email
      ,phone
      ,address
      ,semester
      ,grade
      ,credits
      ,GPA
  FROM university.student student
  LEFT JOIN university.semester_result semester_result
    ON student.id = semester_result.stud_id
  LEFT JOIN university.college college
    ON student.coll_id = college.col_id
 ORDER BY college.col_id AND semester;
   