   SELECT  roll_no
		  ,stud_name
          ,dob
          ,gender
          ,email
          ,phone
          ,address
          ,dept_name
		  ,college_name
      
     FROM university.student student
          LEFT JOIN university.college_department college_department
	      ON student.cdep_id = college_department.cdept_id
          LEFT JOIN university.department department
	      ON college_department.udept_code = department.dept_code
          LEFT JOIN university.college college
	      ON college_department.college_id = college.college_id
	WHERE student.id 
	   IN (SELECT id
			 FROM university.student stud , university.university univ
			WHERE stud.academic_year = '2017'
			  AND univ.university_code = 'A001'
			  AND stud.address ='coimbatore' )
	GROUP BY roll_no ; 