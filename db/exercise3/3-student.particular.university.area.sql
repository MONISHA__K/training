SELECT roll_no
      ,stu_name
      ,gender
      ,student.dob
      ,email
      ,student.phone
      ,address
      ,col_name
      ,dept_name
      ,name as employee_name
  FROM university.student student
  LEFT JOIN university.college_department college_department
    ON student.cdep_id = college_department.cdept_id
  LEFT JOIN university.college college
    ON student.coll_id = college.col_id
 INNER JOIN university.department department
    ON college_department.udept_code = department.dept_code
 INNER JOIN university.employee employee
    ON college.col_id = employee.college_id
 WHERE college.univ_code='A001'
 and student.address='coimbatore'
 and employee.desg_id='2'
 GROUP BY roll_no
 LIMIT 20
