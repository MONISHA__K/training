SELECT roll_no
	  ,stu_name
      ,dob
      ,gender
      ,email
      ,phone
      ,address
      ,dept_name
      ,col_name
  FROM university.student student
  LEFT JOIN university.college_department college_department
    ON student.cdep_id = college_department.cdept_id
  LEFT JOIN university.department department
    ON college_department.udept_code = department.dept_code
  LEFT JOIN university.college college
    ON college_department.college_id = college.col_id
 WHERE student.id 
    IN (SELECT id
          FROM university.student s, university.university u
		 WHERE s.academic_year = '2017'
 and u.university_code = 'A001'
 and s.address ='coimbatore' )
 GROUP BY roll_no        ; 