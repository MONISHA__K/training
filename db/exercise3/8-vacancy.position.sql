use `university`;
SELECT designation.desg_name 
  ,designation.desgn_rank
  ,university.university_code
  ,college.col_name AS college_name
  ,department.dept_name
  ,university.university_name
  ,college.city
  ,college.state
  ,college.year_opened
 FROM university 
  ,college 
  ,department 
  ,designation 
  ,college_department 
  ,employee  
 WHERE college.univ_code = university.university_code 
 AND university.university_code = department.univ_code 
 AND college_department.college_id = college.col_id 
 AND college_department.udept_code = department.dept_code
 AND employee.cdept_id = college_department.cdept_id 
 AND employee.desg_id = designation.desg_id 
 ORDER BY designation.desgn_rank;