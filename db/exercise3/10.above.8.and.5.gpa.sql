    SELECT  student.id
	       ,roll_no
           ,stud_name
		   ,gender
           ,semester
           ,grade
           ,credits
           ,GPA
      FROM university.student student
           LEFT JOIN university.semester_result semester_result 
           ON student.id = semester_result.stud_id
     WHERE GPA >8
        OR GPA >5
     ORDER BY id ;

