 select u.university_name
  ,s.id as roll_number
  ,s.stu_name as student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.col_name as college_name
  ,d.dept_name
  ,sf.amount
  ,sf.paid_year
  ,sf.paid_status
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
  ,syllabus sy
  ,semester_fee sf
 where c.univ_code = u.university_code 
 and u.university_code = d.univ_code 
 and cd.college_id = c.col_id 
 and cd.udept_code = d.dept_code
 and s.coll_id = c.col_id
 and s.cdep_id = cd.cdept_id
 and sy.cdept_id = cd.cdept_id
 and sf.stu_id = s.id
 and sf.cdept_id = cd.cdept_id
 and paid_year = 2019
 order by roll_no ;
 select u.university_name
  ,s.id as roll_number
  ,s.name as student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.col_name as college_name
  ,d.dept_name
  ,sf.amount
  ,sf.paid_year
  ,sf.paid_status
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
  ,syllabus sy
  ,semester_fee sf
 where c.univ_code = u.university_code 
 and u.university_code = d.univ_code 
 and cd.college_id = c.col_id 
 and cd.udept_code = d.dept_code
 and s.college_id = c.col_id
 and s.cdept_id = cd.cdept_id
 and sy.cdept_id = cd.cdept_id
 and sf.stud_id = s.id
 and sf.cdept_id = cd.cdept_id
 and u.university_code=1
 and paid_year = 2020
 order by roll_no; 