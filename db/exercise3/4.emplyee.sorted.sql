
use `university`;
select u.university_code
  ,c.col_name
  ,u.university_name
  ,c.city
  ,c.state
  ,c.year_opened
  ,d.dept_name
  ,e.name as employee_name
  ,de.desg_name as designation_name
  ,de.desgn_rank as employee_rank
 from university u
  ,college c
  ,department d 
  ,designation de
  ,college_department cd
  ,employee e 
 where c.univ_code = u.university_code 
 and u.university_code = d.univ_code 
 and cd.college_id = c.col_id 
 and cd.udept_code = d.dept_code
 and e.college_id = c.col_id 
 and e.cdept_id = cd.cdept_id 
 and e.desg_id = de.desg_id 
 and u.university_code = 1
 order by de.desgn_rank;