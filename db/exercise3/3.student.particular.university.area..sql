     SELECT  roll_no
			,stud_name
            ,gender
            ,student.dob
            ,email
            ,student.phone
            ,address
            ,college_name
            ,dept_name
            ,name as employee_name
       FROM university.student student
            LEFT JOIN  university.college_department college_department
            ON student.cdept_id = college_department.cdept_id
            LEFT JOIN university.college college
            ON student.college_id = college.college_id
            INNER JOIN university.department department
            ON college_department.udept_code = department.dept_code
            INNER JOIN university.employee employee
            ON college.college_id = employee.college_id
	  WHERE college.univ_code='A001'
	    AND student.address='coimbatore'
        AND employee.desgn_id='2'
      GROUP BY roll_no
      LIMIT 20;
