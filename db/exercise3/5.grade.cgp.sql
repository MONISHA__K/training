  select u.university_name
  ,s.roll_no as roll_no
  ,s.stu_name as stu_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.col_name as college_name
  ,d.dept_name
  ,sr.semester
  ,sr.grade
  ,sr.credits
 from university u
  ,college c
  ,department d 
  ,designation de
  ,college_department cd
  ,employee e 
  ,student s
  ,syllabus sy
  ,professor_syllabus ps
  ,semester_result sr
 where c.univ_code = u.university_code 
 and u.university_code = d.univ_code 
 and cd.college_id = c.col_id 
 and cd.udept_code = d.dept_code
 and e.college_id = c.col_id 
 and e.cdept_id = cd.cdept_id 
 and e.desg_id = de.desg_id 
 and s.coll_id = c.col_id
 and s.cdep_id = cd.cdept_id
 and sy.cdept_id = cd.cdept_id
 and ps.syllabus_id = sy.id
 and sr.syllabus_id = sy.id
 and sr.stud_id = s.id
 order by semester ;