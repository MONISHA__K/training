use `university`;

SELECT student.id
	  ,student.roll_no
      ,student.stu_name AS STUDENT_NAME
      ,student.gender
      ,college.col_code
      ,college.col_name AS COLLEGE_NAME
      ,semester_result.grade
      ,semester_result.GPA
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.university_code = college.univ_code
   AND student.coll_id = college.col_id
   AND semester_result.stud_id = student.id
   AND semester_result.GPA > 8
ORDER BY semester_result.GPA;
 use `university`;  
SELECT student.stu_id
	  ,student.roll_no
      ,student.stu_name AS STUDENT_NAME
      ,student.gender
      ,college.college_code
      ,college.college_name AS COLLEGE_NAME
      ,semester_result.grade
      ,semester_result.GPA
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.university_code = college.univ_code
   AND student.coll_id = college.col_id
   AND semester_result.stud_id = student.stu_id
   AND semester_result.GPA > 5
ORDER BY semester_result.GPA;